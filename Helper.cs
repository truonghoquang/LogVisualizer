﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using ZedGraph;
using System.Drawing.Drawing2D;

namespace LogVisualizer
{
    public static class Helper
    {
        public static Color CLR_SET = Color.Black;
        public static Color CLR_CREATE = Color.Orange;
        public static Color CLR_ADD = Color.Blue;
        public static Color CLR_MOVE_TO = Color.Green;
        public static Color CLR_MOVE_FROM = Color.Purple;
        public static Color CLR_REMOVE = Color.Red;
        public static Color CLR_READING = Color.DodgerBlue;
        public static Color CLR_BFREADING = Color.Cyan;

        public static Color CLR_CLASS = Color.Orange;
        public static Color CLR_OPERATION = Color.LightGreen;
        public static Color CLR_ATTRIBUTE = Color.Blue;
        public static Color CLR_ASSOCIATION = Color.Red;
        public static Color CLR_PACKAGE = Color.Black;

        public static Color CLR_EMPHASIS = Color.Red;
        public static SymbolType SYB_EMPHASIS = SymbolType.Circle;
        public static float SIZE_EMPHASIS = 10.0f;
        public static float LINEWIDTH_EMPHASIS = 3.0f;

        public static SymbolType SYB_CLASS = SymbolType.Square;
        public static SymbolType SYB_ASSOC = SymbolType.HDash;
        public static SymbolType SYB_OPERATION = SymbolType.Circle;
        public static SymbolType SYB_ATTR = SymbolType.Plus;
        public static SymbolType SYB_PACKAGE = SymbolType.Diamond;
        public static SymbolType SYB_READING = SymbolType.Triangle;
        public static SymbolType SYB_BFREADING = SymbolType.Triangle;

        public static float SIZE_SYB_CLASS = 2.0f;
        public static float SIZE_SYB_ASSOC = 2.0f;
        public static float SIZE_SYB_OPERATION = 2.0f;
        public static float SIZE_SYB_ATTR = 2.0f;
        public static float SIZE_SYB_PACKAGE = 2.0f;
        public static float SIZE_SYB_READING = 2.0f;
        public static float SIZE_SYB_BFREADING = 2.0f;

        public static float SIZE_SYB = 1.1f;
        public static GraphicsPath SYB_USERDEFINED = new GraphicsPath(
                new[]
                    {
                        new PointF(-0.1f * SIZE_SYB, -0.1f * SIZE_SYB), 
                        new PointF(-0.1f * SIZE_SYB, 0.1f * SIZE_SYB), 
                        new PointF(0.1f * SIZE_SYB, 0.1f * SIZE_SYB), 
                        new PointF(0.1f * SIZE_SYB, -0.1f * SIZE_SYB), 
                        new PointF(-0.1f * SIZE_SYB, -0.1f * SIZE_SYB)
                    },
                new[]
                    {
                        (byte)PathPointType.Start, (byte)PathPointType.Line, 
                        (byte)PathPointType.Line, (byte)PathPointType.Line, (byte)PathPointType.Line
                    });

        public static Double offset_LOG = 1.0;
        public static Double offset_SET = 0.0;
        public static Double offset_CREATE = 0.1;
        public static Double offset_ADD = 0.2;
        public static Double offset_MOVE_TO = 0.3;
        public static Double offset_MOVE_FROM = 0.4;
        public static Double offset_REMOVE = 0.5;
        public static Double offset_READING = 0.6;
        public static Double offset_BFREADING = 0.7;

        public static double offset_CLASS = 0.0;
        public static double offset_OPERATION = 0.2;
        public static double offset_ATTRIBUTE = 0.4;
        public static double offset_ASSOCIATION = 0.6;
        public static double offset_PACKAGE = 0.8;

        public static Double XMax = 10;
        public static Double XMin = 0;
        public static Double YMax = 10;
        public static Double YMin = -0.5;

        public static string STRATEGY_FILE_NAME = "creationString.csv";
    }

    public class EmphasizedPoint 
    {
        LineItem line;
 
        bool visible;

        public EmphasizedPoint(double _x, double _y, Color _color, SymbolType _symbolType, float _symbolSize, float _symbolWidth) 
        {
            this.line = new LineItem("",
                           new double[] { _x },
                           new double[] { _y },
                           _color, _symbolType);
            this.line.Symbol.Size = _symbolSize;
            this.line.Symbol.Border.Width = _symbolWidth;
            this.line.Symbol.Fill = new Fill(Color.Transparent);

            this.visible = true;
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public LineItem Line
        {
            get { return line; }
        }
    }
}
