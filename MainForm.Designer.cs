namespace LogVisualizer
{
    partial class frmUMLLogVisualizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUMLLogVisualizer));
            this.tabLoadFile = new System.Windows.Forms.TabPage();
            this.txtHovering = new System.Windows.Forms.TextBox();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnLoadMultipleLogFile = new System.Windows.Forms.Button();
            this.btnLoadFolder = new System.Windows.Forms.Button();
            this.btnRedraw = new System.Windows.Forms.Button();
            this.tabLinePlot = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.txtYUnit = new System.Windows.Forms.TextBox();
            this.txtXUnit = new System.Windows.Forms.TextBox();
            this.txtYTitle = new System.Windows.Forms.TextBox();
            this.txtXTitle = new System.Windows.Forms.TextBox();
            this.txtPlotTitle = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabController = new System.Windows.Forms.TabControl();
            this.grpGraphicControler = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sizePACKAGE = new System.Windows.Forms.NumericUpDown();
            this.sizeASSOCIATION = new System.Windows.Forms.NumericUpDown();
            this.sizeATTRIBUTE = new System.Windows.Forms.NumericUpDown();
            this.sizeOPERARTION = new System.Windows.Forms.NumericUpDown();
            this.sizeCLASS = new System.Windows.Forms.NumericUpDown();
            this.clpkPACKAGE = new PJLControls.ColorPicker();
            this.cbxAttrColoringMode = new System.Windows.Forms.CheckBox();
            this.clpkASSOCIATION = new PJLControls.ColorPicker();
            this.cbbPACKAGE = new System.Windows.Forms.ComboBox();
            this.clpkATTRIBUTE = new PJLControls.ColorPicker();
            this.cbbASSOCIATION = new System.Windows.Forms.ComboBox();
            this.clpkOPERATION = new PJLControls.ColorPicker();
            this.cbbATTRIBUTE = new System.Windows.Forms.ComboBox();
            this.clpkCLASS = new PJLControls.ColorPicker();
            this.cbbOPERATION = new System.Windows.Forms.ComboBox();
            this.cbbCLASS = new System.Windows.Forms.ComboBox();
            this.ckbPACKAGE = new System.Windows.Forms.CheckBox();
            this.ckbATTRIBUTE = new System.Windows.Forms.CheckBox();
            this.ckbCLASS = new System.Windows.Forms.CheckBox();
            this.ckbASSOCIATION = new System.Windows.Forms.CheckBox();
            this.ckbOPERATION = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvTimeMeasurement = new System.Windows.Forms.DataGridView();
            this.cl_Invisible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cl_Remove = new System.Windows.Forms.DataGridViewImageColumn();
            this.cl_Points = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_TimeMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_Y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpActivitiesSelection = new System.Windows.Forms.GroupBox();
            this.clpkREMOVE = new PJLControls.ColorPicker();
            this.clpkMOVEFROM = new PJLControls.ColorPicker();
            this.clpkMOVETO = new PJLControls.ColorPicker();
            this.clpkADD = new PJLControls.ColorPicker();
            this.clpkCREATE = new PJLControls.ColorPicker();
            this.clpkSET = new PJLControls.ColorPicker();
            this.ckbCREATE = new System.Windows.Forms.CheckBox();
            this.ckbADD = new System.Windows.Forms.CheckBox();
            this.ckbREMOVE = new System.Windows.Forms.CheckBox();
            this.ckbMOVE_FROM = new System.Windows.Forms.CheckBox();
            this.ckbMOVE_TO = new System.Windows.Forms.CheckBox();
            this.ckbSET = new System.Windows.Forms.CheckBox();
            this.grpFiles = new System.Windows.Forms.GroupBox();
            this.cklLogFileSelection = new System.Windows.Forms.CheckedListBox();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.zg1 = new ZedGraph.ZedGraphControl();
            this.clpkREAD = new PJLControls.ColorPicker();
            this.ckbREAD = new System.Windows.Forms.CheckBox();
            this.clpkBFREAD = new PJLControls.ColorPicker();
            this.ckbBFREAD = new System.Windows.Forms.CheckBox();
            this.ttDesignImage = new LogVisualizer.CustomizedToolTip();
            this.tabLoadFile.SuspendLayout();
            this.tabLinePlot.SuspendLayout();
            this.tabController.SuspendLayout();
            this.grpGraphicControler.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizePACKAGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeASSOCIATION)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeATTRIBUTE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOPERARTION)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeCLASS)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTimeMeasurement)).BeginInit();
            this.grpActivitiesSelection.SuspendLayout();
            this.grpFiles.SuspendLayout();
            this.grpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabLoadFile
            // 
            this.tabLoadFile.Controls.Add(this.txtHovering);
            this.tabLoadFile.Controls.Add(this.btnRemoveAll);
            this.tabLoadFile.Controls.Add(this.btnLoadMultipleLogFile);
            this.tabLoadFile.Controls.Add(this.btnLoadFolder);
            this.tabLoadFile.Controls.Add(this.btnRedraw);
            this.tabLoadFile.Location = new System.Drawing.Point(4, 22);
            this.tabLoadFile.Name = "tabLoadFile";
            this.tabLoadFile.Padding = new System.Windows.Forms.Padding(3);
            this.tabLoadFile.Size = new System.Drawing.Size(636, 88);
            this.tabLoadFile.TabIndex = 2;
            this.tabLoadFile.Text = "Load File(s)";
            this.tabLoadFile.UseVisualStyleBackColor = true;
            // 
            // txtHovering
            // 
            this.txtHovering.Location = new System.Drawing.Point(294, 14);
            this.txtHovering.Name = "txtHovering";
            this.txtHovering.Size = new System.Drawing.Size(174, 20);
            this.txtHovering.TabIndex = 11;
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(138, 40);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(90, 23);
            this.btnRemoveAll.TabIndex = 16;
            this.btnRemoveAll.Text = "Remove All";
            this.btnRemoveAll.UseVisualStyleBackColor = true;
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnLoadMultipleLogFile
            // 
            this.btnLoadMultipleLogFile.Location = new System.Drawing.Point(5, 40);
            this.btnLoadMultipleLogFile.Name = "btnLoadMultipleLogFile";
            this.btnLoadMultipleLogFile.Size = new System.Drawing.Size(105, 23);
            this.btnLoadMultipleLogFile.TabIndex = 16;
            this.btnLoadMultipleLogFile.Text = "Add log files";
            this.btnLoadMultipleLogFile.UseVisualStyleBackColor = true;
            this.btnLoadMultipleLogFile.Click += new System.EventHandler(this.btnLoadMultipleLogFiles_Click);
            // 
            // btnLoadFolder
            // 
            this.btnLoadFolder.Location = new System.Drawing.Point(5, 11);
            this.btnLoadFolder.Name = "btnLoadFolder";
            this.btnLoadFolder.Size = new System.Drawing.Size(105, 23);
            this.btnLoadFolder.TabIndex = 16;
            this.btnLoadFolder.Text = "Load Folder";
            this.btnLoadFolder.UseVisualStyleBackColor = true;
            this.btnLoadFolder.Click += new System.EventHandler(this.btnLoadFromFolder_Click);
            // 
            // btnRedraw
            // 
            this.btnRedraw.Location = new System.Drawing.Point(138, 11);
            this.btnRedraw.Name = "btnRedraw";
            this.btnRedraw.Size = new System.Drawing.Size(90, 23);
            this.btnRedraw.TabIndex = 8;
            this.btnRedraw.Text = "Redraw";
            this.btnRedraw.UseVisualStyleBackColor = true;
            this.btnRedraw.Click += new System.EventHandler(this.btnRedraw_Click);
            // 
            // tabLinePlot
            // 
            this.tabLinePlot.Controls.Add(this.label5);
            this.tabLinePlot.Controls.Add(this.txtYUnit);
            this.tabLinePlot.Controls.Add(this.txtXUnit);
            this.tabLinePlot.Controls.Add(this.txtYTitle);
            this.tabLinePlot.Controls.Add(this.txtXTitle);
            this.tabLinePlot.Controls.Add(this.txtPlotTitle);
            this.tabLinePlot.Controls.Add(this.label6);
            this.tabLinePlot.Controls.Add(this.label3);
            this.tabLinePlot.Controls.Add(this.label2);
            this.tabLinePlot.Controls.Add(this.label1);
            this.tabLinePlot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabLinePlot.Location = new System.Drawing.Point(4, 22);
            this.tabLinePlot.Name = "tabLinePlot";
            this.tabLinePlot.Padding = new System.Windows.Forms.Padding(3);
            this.tabLinePlot.Size = new System.Drawing.Size(636, 88);
            this.tabLinePlot.TabIndex = 0;
            this.tabLinePlot.Text = "Graph Settings";
            this.tabLinePlot.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Y Axis Unit";
            // 
            // txtYUnit
            // 
            this.txtYUnit.Location = new System.Drawing.Point(287, 59);
            this.txtYUnit.Name = "txtYUnit";
            this.txtYUnit.Size = new System.Drawing.Size(100, 20);
            this.txtYUnit.TabIndex = 6;
            this.txtYUnit.Text = "of Students";
            // 
            // txtXUnit
            // 
            this.txtXUnit.Location = new System.Drawing.Point(72, 59);
            this.txtXUnit.Name = "txtXUnit";
            this.txtXUnit.Size = new System.Drawing.Size(100, 20);
            this.txtXUnit.TabIndex = 5;
            this.txtXUnit.Text = "second";
            // 
            // txtYTitle
            // 
            this.txtYTitle.Location = new System.Drawing.Point(286, 34);
            this.txtYTitle.Name = "txtYTitle";
            this.txtYTitle.Size = new System.Drawing.Size(100, 20);
            this.txtYTitle.TabIndex = 4;
            this.txtYTitle.Text = "Logs";
            // 
            // txtXTitle
            // 
            this.txtXTitle.Location = new System.Drawing.Point(73, 34);
            this.txtXTitle.Name = "txtXTitle";
            this.txtXTitle.Size = new System.Drawing.Size(100, 20);
            this.txtXTitle.TabIndex = 3;
            this.txtXTitle.Text = "Time";
            // 
            // txtPlotTitle
            // 
            this.txtPlotTitle.Location = new System.Drawing.Point(73, 7);
            this.txtPlotTitle.Name = "txtPlotTitle";
            this.txtPlotTitle.Size = new System.Drawing.Size(313, 20);
            this.txtPlotTitle.TabIndex = 2;
            this.txtPlotTitle.Text = "Logs Visualisation";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(222, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Y Axis Title";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "X Axis Unit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "X Axis Title";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Plot Title";
            // 
            // tabController
            // 
            this.tabController.Controls.Add(this.tabLoadFile);
            this.tabController.Controls.Add(this.tabLinePlot);
            this.tabController.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabController.Location = new System.Drawing.Point(3, 16);
            this.tabController.Name = "tabController";
            this.tabController.SelectedIndex = 0;
            this.tabController.Size = new System.Drawing.Size(644, 114);
            this.tabController.TabIndex = 8;
            // 
            // grpGraphicControler
            // 
            this.grpGraphicControler.Controls.Add(this.groupBox4);
            this.grpGraphicControler.Controls.Add(this.groupBox1);
            this.grpGraphicControler.Controls.Add(this.grpActivitiesSelection);
            this.grpGraphicControler.Controls.Add(this.grpFiles);
            this.grpGraphicControler.Dock = System.Windows.Forms.DockStyle.Right;
            this.grpGraphicControler.Location = new System.Drawing.Point(650, 0);
            this.grpGraphicControler.Name = "grpGraphicControler";
            this.grpGraphicControler.Size = new System.Drawing.Size(324, 738);
            this.grpGraphicControler.TabIndex = 10;
            this.grpGraphicControler.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.sizePACKAGE);
            this.groupBox4.Controls.Add(this.sizeASSOCIATION);
            this.groupBox4.Controls.Add(this.sizeATTRIBUTE);
            this.groupBox4.Controls.Add(this.sizeOPERARTION);
            this.groupBox4.Controls.Add(this.sizeCLASS);
            this.groupBox4.Controls.Add(this.clpkPACKAGE);
            this.groupBox4.Controls.Add(this.cbxAttrColoringMode);
            this.groupBox4.Controls.Add(this.clpkASSOCIATION);
            this.groupBox4.Controls.Add(this.cbbPACKAGE);
            this.groupBox4.Controls.Add(this.clpkATTRIBUTE);
            this.groupBox4.Controls.Add(this.cbbASSOCIATION);
            this.groupBox4.Controls.Add(this.clpkOPERATION);
            this.groupBox4.Controls.Add(this.cbbATTRIBUTE);
            this.groupBox4.Controls.Add(this.clpkCLASS);
            this.groupBox4.Controls.Add(this.cbbOPERATION);
            this.groupBox4.Controls.Add(this.cbbCLASS);
            this.groupBox4.Controls.Add(this.ckbPACKAGE);
            this.groupBox4.Controls.Add(this.ckbATTRIBUTE);
            this.groupBox4.Controls.Add(this.ckbCLASS);
            this.groupBox4.Controls.Add(this.ckbASSOCIATION);
            this.groupBox4.Controls.Add(this.ckbOPERATION);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 353);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(318, 166);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Attribute Types Selection";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(106, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Symbols";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(178, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Size";
            // 
            // sizePACKAGE
            // 
            this.sizePACKAGE.Location = new System.Drawing.Point(181, 139);
            this.sizePACKAGE.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.sizePACKAGE.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizePACKAGE.Name = "sizePACKAGE";
            this.sizePACKAGE.Size = new System.Drawing.Size(38, 20);
            this.sizePACKAGE.TabIndex = 13;
            this.sizePACKAGE.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.sizePACKAGE.ValueChanged += new System.EventHandler(this.sizePACKAGE_ValueChanged);
            // 
            // sizeASSOCIATION
            // 
            this.sizeASSOCIATION.Location = new System.Drawing.Point(181, 116);
            this.sizeASSOCIATION.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.sizeASSOCIATION.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeASSOCIATION.Name = "sizeASSOCIATION";
            this.sizeASSOCIATION.Size = new System.Drawing.Size(38, 20);
            this.sizeASSOCIATION.TabIndex = 13;
            this.sizeASSOCIATION.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.sizeASSOCIATION.ValueChanged += new System.EventHandler(this.sizeASSOCIATION_ValueChanged);
            // 
            // sizeATTRIBUTE
            // 
            this.sizeATTRIBUTE.Location = new System.Drawing.Point(181, 91);
            this.sizeATTRIBUTE.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.sizeATTRIBUTE.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeATTRIBUTE.Name = "sizeATTRIBUTE";
            this.sizeATTRIBUTE.Size = new System.Drawing.Size(38, 20);
            this.sizeATTRIBUTE.TabIndex = 13;
            this.sizeATTRIBUTE.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.sizeATTRIBUTE.ValueChanged += new System.EventHandler(this.sizeATTRIBUTE_ValueChanged);
            // 
            // sizeOPERARTION
            // 
            this.sizeOPERARTION.Location = new System.Drawing.Point(181, 68);
            this.sizeOPERARTION.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.sizeOPERARTION.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeOPERARTION.Name = "sizeOPERARTION";
            this.sizeOPERARTION.Size = new System.Drawing.Size(38, 20);
            this.sizeOPERARTION.TabIndex = 13;
            this.sizeOPERARTION.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.sizeOPERARTION.ValueChanged += new System.EventHandler(this.sizeOPERARTION_ValueChanged);
            // 
            // sizeCLASS
            // 
            this.sizeCLASS.Location = new System.Drawing.Point(180, 45);
            this.sizeCLASS.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.sizeCLASS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeCLASS.Name = "sizeCLASS";
            this.sizeCLASS.Size = new System.Drawing.Size(38, 20);
            this.sizeCLASS.TabIndex = 13;
            this.sizeCLASS.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.sizeCLASS.ValueChanged += new System.EventHandler(this.sizeCLASS_ValueChanged);
            // 
            // clpkPACKAGE
            // 
            this.clpkPACKAGE.Color = System.Drawing.Color.Red;
            this.clpkPACKAGE.Enabled = false;
            this.clpkPACKAGE.Location = new System.Drawing.Point(219, 139);
            this.clpkPACKAGE.Name = "clpkPACKAGE";
            this.clpkPACKAGE.Size = new System.Drawing.Size(86, 19);
            this.clpkPACKAGE.TabIndex = 9;
            this.clpkPACKAGE.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkPACKAGE_ColorChanged);
            // 
            // cbxAttrColoringMode
            // 
            this.cbxAttrColoringMode.AutoSize = true;
            this.cbxAttrColoringMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxAttrColoringMode.Location = new System.Drawing.Point(220, 21);
            this.cbxAttrColoringMode.Margin = new System.Windows.Forms.Padding(2);
            this.cbxAttrColoringMode.Name = "cbxAttrColoringMode";
            this.cbxAttrColoringMode.Size = new System.Drawing.Size(94, 17);
            this.cbxAttrColoringMode.TabIndex = 5;
            this.cbxAttrColoringMode.Text = "Coloring Mode";
            this.cbxAttrColoringMode.UseVisualStyleBackColor = true;
            this.cbxAttrColoringMode.CheckedChanged += new System.EventHandler(this.cbxAttrColoringMode_CheckedChanged);
            // 
            // clpkASSOCIATION
            // 
            this.clpkASSOCIATION.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.clpkASSOCIATION.Enabled = false;
            this.clpkASSOCIATION.Location = new System.Drawing.Point(219, 116);
            this.clpkASSOCIATION.Name = "clpkASSOCIATION";
            this.clpkASSOCIATION.Size = new System.Drawing.Size(86, 19);
            this.clpkASSOCIATION.TabIndex = 7;
            this.clpkASSOCIATION.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkASSOCIATION_ColorChanged);
            // 
            // cbbPACKAGE
            // 
            this.cbbPACKAGE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbPACKAGE.FormattingEnabled = true;
            this.cbbPACKAGE.Location = new System.Drawing.Point(109, 139);
            this.cbbPACKAGE.Name = "cbbPACKAGE";
            this.cbbPACKAGE.Size = new System.Drawing.Size(72, 21);
            this.cbbPACKAGE.TabIndex = 4;
            this.cbbPACKAGE.SelectedIndexChanged += new System.EventHandler(this.cbbPACKAGE_SelectedIndexChanged);
            // 
            // clpkATTRIBUTE
            // 
            this.clpkATTRIBUTE.Color = System.Drawing.Color.Green;
            this.clpkATTRIBUTE.Enabled = false;
            this.clpkATTRIBUTE.Location = new System.Drawing.Point(219, 92);
            this.clpkATTRIBUTE.Name = "clpkATTRIBUTE";
            this.clpkATTRIBUTE.Size = new System.Drawing.Size(86, 19);
            this.clpkATTRIBUTE.TabIndex = 11;
            this.clpkATTRIBUTE.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkATTRIBUTE_ColorChanged);
            // 
            // cbbASSOCIATION
            // 
            this.cbbASSOCIATION.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbASSOCIATION.FormattingEnabled = true;
            this.cbbASSOCIATION.Location = new System.Drawing.Point(109, 115);
            this.cbbASSOCIATION.Name = "cbbASSOCIATION";
            this.cbbASSOCIATION.Size = new System.Drawing.Size(72, 21);
            this.cbbASSOCIATION.TabIndex = 4;
            this.cbbASSOCIATION.SelectedIndexChanged += new System.EventHandler(this.cbbASSOCIATION_SelectedIndexChanged);
            // 
            // clpkOPERATION
            // 
            this.clpkOPERATION.Color = System.Drawing.Color.Blue;
            this.clpkOPERATION.Enabled = false;
            this.clpkOPERATION.Location = new System.Drawing.Point(219, 68);
            this.clpkOPERATION.Name = "clpkOPERATION";
            this.clpkOPERATION.Size = new System.Drawing.Size(86, 19);
            this.clpkOPERATION.TabIndex = 6;
            this.clpkOPERATION.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkOPERATION_ColorChanged);
            // 
            // cbbATTRIBUTE
            // 
            this.cbbATTRIBUTE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbATTRIBUTE.FormattingEnabled = true;
            this.cbbATTRIBUTE.Location = new System.Drawing.Point(109, 91);
            this.cbbATTRIBUTE.Name = "cbbATTRIBUTE";
            this.cbbATTRIBUTE.Size = new System.Drawing.Size(72, 21);
            this.cbbATTRIBUTE.TabIndex = 4;
            this.cbbATTRIBUTE.SelectedIndexChanged += new System.EventHandler(this.cbbATTRIBUTE_SelectedIndexChanged);
            // 
            // clpkCLASS
            // 
            this.clpkCLASS.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.clpkCLASS.Enabled = false;
            this.clpkCLASS.Location = new System.Drawing.Point(220, 44);
            this.clpkCLASS.Name = "clpkCLASS";
            this.clpkCLASS.Size = new System.Drawing.Size(86, 19);
            this.clpkCLASS.TabIndex = 10;
            this.clpkCLASS.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkCLASS_ColorChanged);
            // 
            // cbbOPERATION
            // 
            this.cbbOPERATION.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbOPERATION.FormattingEnabled = true;
            this.cbbOPERATION.Location = new System.Drawing.Point(109, 67);
            this.cbbOPERATION.Name = "cbbOPERATION";
            this.cbbOPERATION.Size = new System.Drawing.Size(72, 21);
            this.cbbOPERATION.TabIndex = 4;
            this.cbbOPERATION.SelectedIndexChanged += new System.EventHandler(this.cbbOPERATION_SelectedIndexChanged);
            // 
            // cbbCLASS
            // 
            this.cbbCLASS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbCLASS.FormattingEnabled = true;
            this.cbbCLASS.Location = new System.Drawing.Point(109, 44);
            this.cbbCLASS.Name = "cbbCLASS";
            this.cbbCLASS.Size = new System.Drawing.Size(72, 21);
            this.cbbCLASS.TabIndex = 4;
            this.cbbCLASS.SelectedIndexChanged += new System.EventHandler(this.cbbCLASS_SelectedIndexChanged);
            // 
            // ckbPACKAGE
            // 
            this.ckbPACKAGE.AutoSize = true;
            this.ckbPACKAGE.Checked = true;
            this.ckbPACKAGE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbPACKAGE.Location = new System.Drawing.Point(6, 140);
            this.ckbPACKAGE.Name = "ckbPACKAGE";
            this.ckbPACKAGE.Size = new System.Drawing.Size(76, 17);
            this.ckbPACKAGE.TabIndex = 3;
            this.ckbPACKAGE.Text = "PACKAGE";
            this.ckbPACKAGE.UseVisualStyleBackColor = true;
            this.ckbPACKAGE.CheckedChanged += new System.EventHandler(this.ckbPACKAGE_CheckedChanged);
            // 
            // ckbATTRIBUTE
            // 
            this.ckbATTRIBUTE.AutoSize = true;
            this.ckbATTRIBUTE.Checked = true;
            this.ckbATTRIBUTE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbATTRIBUTE.Location = new System.Drawing.Point(6, 93);
            this.ckbATTRIBUTE.Name = "ckbATTRIBUTE";
            this.ckbATTRIBUTE.Size = new System.Drawing.Size(87, 17);
            this.ckbATTRIBUTE.TabIndex = 2;
            this.ckbATTRIBUTE.Text = "ATTRIBUTE";
            this.ckbATTRIBUTE.UseVisualStyleBackColor = true;
            this.ckbATTRIBUTE.CheckedChanged += new System.EventHandler(this.ckbATTRIBUTE_CheckedChanged);
            // 
            // ckbCLASS
            // 
            this.ckbCLASS.AutoSize = true;
            this.ckbCLASS.Checked = true;
            this.ckbCLASS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbCLASS.Location = new System.Drawing.Point(6, 46);
            this.ckbCLASS.Name = "ckbCLASS";
            this.ckbCLASS.Size = new System.Drawing.Size(60, 17);
            this.ckbCLASS.TabIndex = 1;
            this.ckbCLASS.Text = "CLASS";
            this.ckbCLASS.UseVisualStyleBackColor = true;
            this.ckbCLASS.CheckedChanged += new System.EventHandler(this.ckbCLASS_CheckedChanged);
            // 
            // ckbASSOCIATION
            // 
            this.ckbASSOCIATION.AutoSize = true;
            this.ckbASSOCIATION.Checked = true;
            this.ckbASSOCIATION.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbASSOCIATION.Location = new System.Drawing.Point(6, 117);
            this.ckbASSOCIATION.Name = "ckbASSOCIATION";
            this.ckbASSOCIATION.Size = new System.Drawing.Size(98, 17);
            this.ckbASSOCIATION.TabIndex = 2;
            this.ckbASSOCIATION.Text = "ASSOCIATION";
            this.ckbASSOCIATION.UseVisualStyleBackColor = true;
            this.ckbASSOCIATION.CheckedChanged += new System.EventHandler(this.ckbASSOCIATION_CheckedChanged);
            // 
            // ckbOPERATION
            // 
            this.ckbOPERATION.AutoSize = true;
            this.ckbOPERATION.Checked = true;
            this.ckbOPERATION.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbOPERATION.Location = new System.Drawing.Point(6, 70);
            this.ckbOPERATION.Name = "ckbOPERATION";
            this.ckbOPERATION.Size = new System.Drawing.Size(89, 17);
            this.ckbOPERATION.TabIndex = 2;
            this.ckbOPERATION.Text = "OPERATION";
            this.ckbOPERATION.UseVisualStyleBackColor = true;
            this.ckbOPERATION.CheckedChanged += new System.EventHandler(this.ckbOPERATION_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvTimeMeasurement);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(3, 519);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 216);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Time Measurement";
            // 
            // dgvTimeMeasurement
            // 
            this.dgvTimeMeasurement.AllowUserToAddRows = false;
            this.dgvTimeMeasurement.AllowUserToDeleteRows = false;
            this.dgvTimeMeasurement.AllowUserToResizeColumns = false;
            this.dgvTimeMeasurement.AllowUserToResizeRows = false;
            this.dgvTimeMeasurement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTimeMeasurement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cl_Invisible,
            this.cl_Remove,
            this.cl_Points,
            this.cl_TimeMeasurement,
            this.cl_X,
            this.cl_Y});
            this.dgvTimeMeasurement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTimeMeasurement.Location = new System.Drawing.Point(3, 16);
            this.dgvTimeMeasurement.Margin = new System.Windows.Forms.Padding(2);
            this.dgvTimeMeasurement.Name = "dgvTimeMeasurement";
            this.dgvTimeMeasurement.RowHeadersVisible = false;
            this.dgvTimeMeasurement.RowTemplate.Height = 33;
            this.dgvTimeMeasurement.Size = new System.Drawing.Size(312, 197);
            this.dgvTimeMeasurement.TabIndex = 0;
            this.dgvTimeMeasurement.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTimeMeasurement_CellClick);
            this.dgvTimeMeasurement.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTimeMeasurement_CellValueChanged);
            this.dgvTimeMeasurement.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvTimeMeasurement_CurrentCellDirtyStateChanged);
            // 
            // cl_Invisible
            // 
            this.cl_Invisible.Frozen = true;
            this.cl_Invisible.HeaderText = "";
            this.cl_Invisible.Name = "cl_Invisible";
            this.cl_Invisible.Width = 20;
            // 
            // cl_Remove
            // 
            this.cl_Remove.Frozen = true;
            this.cl_Remove.HeaderText = "";
            this.cl_Remove.Name = "cl_Remove";
            this.cl_Remove.Width = 20;
            // 
            // cl_Points
            // 
            this.cl_Points.Frozen = true;
            this.cl_Points.HeaderText = "Point List";
            this.cl_Points.Name = "cl_Points";
            this.cl_Points.Width = 180;
            // 
            // cl_TimeMeasurement
            // 
            this.cl_TimeMeasurement.Frozen = true;
            this.cl_TimeMeasurement.HeaderText = "Time measurement";
            this.cl_TimeMeasurement.Name = "cl_TimeMeasurement";
            this.cl_TimeMeasurement.Width = 90;
            // 
            // cl_X
            // 
            this.cl_X.HeaderText = "ColX";
            this.cl_X.Name = "cl_X";
            this.cl_X.Visible = false;
            // 
            // cl_Y
            // 
            this.cl_Y.HeaderText = "ColY";
            this.cl_Y.Name = "cl_Y";
            this.cl_Y.Visible = false;
            // 
            // grpActivitiesSelection
            // 
            this.grpActivitiesSelection.Controls.Add(this.clpkBFREAD);
            this.grpActivitiesSelection.Controls.Add(this.ckbBFREAD);
            this.grpActivitiesSelection.Controls.Add(this.clpkREAD);
            this.grpActivitiesSelection.Controls.Add(this.ckbREAD);
            this.grpActivitiesSelection.Controls.Add(this.clpkREMOVE);
            this.grpActivitiesSelection.Controls.Add(this.clpkMOVEFROM);
            this.grpActivitiesSelection.Controls.Add(this.clpkMOVETO);
            this.grpActivitiesSelection.Controls.Add(this.clpkADD);
            this.grpActivitiesSelection.Controls.Add(this.clpkCREATE);
            this.grpActivitiesSelection.Controls.Add(this.clpkSET);
            this.grpActivitiesSelection.Controls.Add(this.ckbCREATE);
            this.grpActivitiesSelection.Controls.Add(this.ckbADD);
            this.grpActivitiesSelection.Controls.Add(this.ckbREMOVE);
            this.grpActivitiesSelection.Controls.Add(this.ckbMOVE_FROM);
            this.grpActivitiesSelection.Controls.Add(this.ckbMOVE_TO);
            this.grpActivitiesSelection.Controls.Add(this.ckbSET);
            this.grpActivitiesSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpActivitiesSelection.Location = new System.Drawing.Point(3, 137);
            this.grpActivitiesSelection.Name = "grpActivitiesSelection";
            this.grpActivitiesSelection.Size = new System.Drawing.Size(318, 216);
            this.grpActivitiesSelection.TabIndex = 9;
            this.grpActivitiesSelection.TabStop = false;
            this.grpActivitiesSelection.Text = " Activities Selection";
            // 
            // clpkREMOVE
            // 
            this.clpkREMOVE.Color = System.Drawing.Color.Red;
            this.clpkREMOVE.Location = new System.Drawing.Point(109, 137);
            this.clpkREMOVE.Name = "clpkREMOVE";
            this.clpkREMOVE.Size = new System.Drawing.Size(196, 19);
            this.clpkREMOVE.TabIndex = 5;
            this.clpkREMOVE.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkREMOVE_ColorChanged);
            // 
            // clpkMOVEFROM
            // 
            this.clpkMOVEFROM.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.clpkMOVEFROM.Location = new System.Drawing.Point(109, 113);
            this.clpkMOVEFROM.Name = "clpkMOVEFROM";
            this.clpkMOVEFROM.Size = new System.Drawing.Size(196, 19);
            this.clpkMOVEFROM.TabIndex = 4;
            this.clpkMOVEFROM.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkMOVEFROM_ColorChanged);
            // 
            // clpkMOVETO
            // 
            this.clpkMOVETO.Color = System.Drawing.Color.Green;
            this.clpkMOVETO.Location = new System.Drawing.Point(109, 89);
            this.clpkMOVETO.Name = "clpkMOVETO";
            this.clpkMOVETO.Size = new System.Drawing.Size(196, 19);
            this.clpkMOVETO.TabIndex = 5;
            this.clpkMOVETO.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkMOVETO_ColorChanged);
            // 
            // clpkADD
            // 
            this.clpkADD.Color = System.Drawing.Color.Blue;
            this.clpkADD.Location = new System.Drawing.Point(109, 65);
            this.clpkADD.Name = "clpkADD";
            this.clpkADD.Size = new System.Drawing.Size(196, 19);
            this.clpkADD.TabIndex = 4;
            this.clpkADD.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkADD_ColorChanged);
            // 
            // clpkCREATE
            // 
            this.clpkCREATE.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.clpkCREATE.Location = new System.Drawing.Point(109, 41);
            this.clpkCREATE.Name = "clpkCREATE";
            this.clpkCREATE.Size = new System.Drawing.Size(196, 19);
            this.clpkCREATE.TabIndex = 5;
            this.clpkCREATE.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkCREATE_ColorChanged);
            // 
            // clpkSET
            // 
            this.clpkSET.Location = new System.Drawing.Point(109, 17);
            this.clpkSET.Name = "clpkSET";
            this.clpkSET.Size = new System.Drawing.Size(196, 19);
            this.clpkSET.TabIndex = 4;
            this.clpkSET.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkSET_ColorChanged);
            // 
            // ckbCREATE
            // 
            this.ckbCREATE.AutoSize = true;
            this.ckbCREATE.Checked = true;
            this.ckbCREATE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbCREATE.Location = new System.Drawing.Point(6, 42);
            this.ckbCREATE.Name = "ckbCREATE";
            this.ckbCREATE.Size = new System.Drawing.Size(69, 17);
            this.ckbCREATE.TabIndex = 2;
            this.ckbCREATE.Text = "CREATE";
            this.ckbCREATE.UseVisualStyleBackColor = true;
            this.ckbCREATE.CheckedChanged += new System.EventHandler(this.ckbCREATE_CheckedChanged);
            // 
            // ckbADD
            // 
            this.ckbADD.AutoSize = true;
            this.ckbADD.Checked = true;
            this.ckbADD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbADD.Location = new System.Drawing.Point(6, 66);
            this.ckbADD.Name = "ckbADD";
            this.ckbADD.Size = new System.Drawing.Size(49, 17);
            this.ckbADD.TabIndex = 2;
            this.ckbADD.Text = "ADD";
            this.ckbADD.UseVisualStyleBackColor = true;
            this.ckbADD.CheckedChanged += new System.EventHandler(this.ckbADD_CheckedChanged);
            // 
            // ckbREMOVE
            // 
            this.ckbREMOVE.AutoSize = true;
            this.ckbREMOVE.Checked = true;
            this.ckbREMOVE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbREMOVE.Location = new System.Drawing.Point(6, 138);
            this.ckbREMOVE.Name = "ckbREMOVE";
            this.ckbREMOVE.Size = new System.Drawing.Size(72, 17);
            this.ckbREMOVE.TabIndex = 2;
            this.ckbREMOVE.Text = "REMOVE";
            this.ckbREMOVE.UseVisualStyleBackColor = true;
            this.ckbREMOVE.CheckedChanged += new System.EventHandler(this.ckbREMOVE_CheckedChanged);
            // 
            // ckbMOVE_FROM
            // 
            this.ckbMOVE_FROM.AutoSize = true;
            this.ckbMOVE_FROM.Checked = true;
            this.ckbMOVE_FROM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbMOVE_FROM.Location = new System.Drawing.Point(6, 114);
            this.ckbMOVE_FROM.Name = "ckbMOVE_FROM";
            this.ckbMOVE_FROM.Size = new System.Drawing.Size(94, 17);
            this.ckbMOVE_FROM.TabIndex = 1;
            this.ckbMOVE_FROM.Text = "MOVE_FROM";
            this.ckbMOVE_FROM.UseVisualStyleBackColor = true;
            this.ckbMOVE_FROM.CheckedChanged += new System.EventHandler(this.ckbMOVE_FROM_CheckedChanged);
            // 
            // ckbMOVE_TO
            // 
            this.ckbMOVE_TO.AutoSize = true;
            this.ckbMOVE_TO.Checked = true;
            this.ckbMOVE_TO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbMOVE_TO.Location = new System.Drawing.Point(6, 90);
            this.ckbMOVE_TO.Name = "ckbMOVE_TO";
            this.ckbMOVE_TO.Size = new System.Drawing.Size(78, 17);
            this.ckbMOVE_TO.TabIndex = 2;
            this.ckbMOVE_TO.Text = "MOVE_TO";
            this.ckbMOVE_TO.UseVisualStyleBackColor = true;
            this.ckbMOVE_TO.CheckedChanged += new System.EventHandler(this.ckbMOVE_TO_CheckedChanged);
            // 
            // ckbSET
            // 
            this.ckbSET.AutoSize = true;
            this.ckbSET.Checked = true;
            this.ckbSET.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbSET.Location = new System.Drawing.Point(6, 18);
            this.ckbSET.Name = "ckbSET";
            this.ckbSET.Size = new System.Drawing.Size(47, 17);
            this.ckbSET.TabIndex = 1;
            this.ckbSET.Text = "SET";
            this.ckbSET.UseVisualStyleBackColor = true;
            this.ckbSET.CheckedChanged += new System.EventHandler(this.ckbSET_CheckedChanged);
            // 
            // grpFiles
            // 
            this.grpFiles.Controls.Add(this.cklLogFileSelection);
            this.grpFiles.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpFiles.Location = new System.Drawing.Point(3, 16);
            this.grpFiles.Name = "grpFiles";
            this.grpFiles.Size = new System.Drawing.Size(318, 121);
            this.grpFiles.TabIndex = 6;
            this.grpFiles.TabStop = false;
            this.grpFiles.Text = "Logging File Selection";
            // 
            // cklLogFileSelection
            // 
            this.cklLogFileSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cklLogFileSelection.FormattingEnabled = true;
            this.cklLogFileSelection.Location = new System.Drawing.Point(3, 16);
            this.cklLogFileSelection.Name = "cklLogFileSelection";
            this.cklLogFileSelection.Size = new System.Drawing.Size(312, 102);
            this.cklLogFileSelection.TabIndex = 0;
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.zg1);
            this.grpMain.Controls.Add(this.tabController);
            this.grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMain.Location = new System.Drawing.Point(0, 0);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(650, 738);
            this.grpMain.TabIndex = 11;
            this.grpMain.TabStop = false;
            // 
            // zg1
            // 
            this.zg1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zg1.IsEnableVZoom = false;
            this.zg1.Location = new System.Drawing.Point(3, 130);
            this.zg1.Margin = new System.Windows.Forms.Padding(6);
            this.zg1.Name = "zg1";
            this.zg1.ScrollGrace = 0D;
            this.zg1.ScrollMaxX = 0D;
            this.zg1.ScrollMaxY = 0D;
            this.zg1.ScrollMaxY2 = 0D;
            this.zg1.ScrollMinX = 0D;
            this.zg1.ScrollMinY = 0D;
            this.zg1.ScrollMinY2 = 0D;
            this.zg1.Size = new System.Drawing.Size(644, 605);
            this.zg1.TabIndex = 10;
            this.zg1.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zg1_MouseDownEvent);
            // 
            // clpkREAD
            // 
            this.clpkREAD.Color = System.Drawing.Color.DodgerBlue;
            this.clpkREAD.Location = new System.Drawing.Point(109, 162);
            this.clpkREAD.Name = "clpkREAD";
            this.clpkREAD.Size = new System.Drawing.Size(196, 19);
            this.clpkREAD.TabIndex = 7;
            this.clpkREAD.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkREAD_ColorChanged);
            // 
            // ckbREAD
            // 
            this.ckbREAD.AutoSize = true;
            this.ckbREAD.Checked = true;
            this.ckbREAD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbREAD.Location = new System.Drawing.Point(6, 163);
            this.ckbREAD.Name = "ckbREAD";
            this.ckbREAD.Size = new System.Drawing.Size(75, 17);
            this.ckbREAD.TabIndex = 6;
            this.ckbREAD.Text = "READING";
            this.ckbREAD.UseVisualStyleBackColor = true;
            this.ckbREAD.CheckedChanged += new System.EventHandler(this.ckbREAD_CheckedChanged);
            // 
            // clpkBFREAD
            // 
            this.clpkBFREAD.Color = System.Drawing.Color.Cyan;
            this.clpkBFREAD.Location = new System.Drawing.Point(109, 187);
            this.clpkBFREAD.Name = "clpkBFREAD";
            this.clpkBFREAD.Size = new System.Drawing.Size(196, 19);
            this.clpkBFREAD.TabIndex = 9;
            this.clpkBFREAD.ColorChanged += new PJLControls.ColorChangedEventHandler(this.clpkBFREAD_ColorChanged);
            // 
            // ckbBFREAD
            // 
            this.ckbBFREAD.AutoSize = true;
            this.ckbBFREAD.Checked = true;
            this.ckbBFREAD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbBFREAD.Location = new System.Drawing.Point(6, 188);
            this.ckbBFREAD.Name = "ckbBFREAD";
            this.ckbBFREAD.Size = new System.Drawing.Size(94, 17);
            this.ckbBFREAD.TabIndex = 8;
            this.ckbBFREAD.Text = "BF_READING";
            this.ckbBFREAD.UseVisualStyleBackColor = true;
            this.ckbBFREAD.CheckedChanged += new System.EventHandler(this.ckbBFREAD_CheckedChanged);
            // 
            // ttDesignImage
            // 
            this.ttDesignImage.AutoSize = false;
            this.ttDesignImage.BorderColor = System.Drawing.Color.Red;
            this.ttDesignImage.OwnerDraw = true;
            this.ttDesignImage.Size = new System.Drawing.Size(400, 150);
            // 
            // frmUMLLogVisualizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 738);
            this.Controls.Add(this.grpMain);
            this.Controls.Add(this.grpGraphicControler);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(537, 437);
            this.Name = "frmUMLLogVisualizer";
            this.Text = "UML Log Visualizer";
            this.tabLoadFile.ResumeLayout(false);
            this.tabLoadFile.PerformLayout();
            this.tabLinePlot.ResumeLayout(false);
            this.tabLinePlot.PerformLayout();
            this.tabController.ResumeLayout(false);
            this.grpGraphicControler.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizePACKAGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeASSOCIATION)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeATTRIBUTE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOPERARTION)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeCLASS)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTimeMeasurement)).EndInit();
            this.grpActivitiesSelection.ResumeLayout(false);
            this.grpActivitiesSelection.PerformLayout();
            this.grpFiles.ResumeLayout(false);
            this.grpMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabLoadFile;
        private System.Windows.Forms.TabPage tabLinePlot;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtYUnit;
        private System.Windows.Forms.TextBox txtXUnit;
        private System.Windows.Forms.TextBox txtYTitle;
        private System.Windows.Forms.TextBox txtXTitle;
        private System.Windows.Forms.TextBox txtPlotTitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRedraw;
        private System.Windows.Forms.TabControl tabController;
        private System.Windows.Forms.Button btnLoadFolder;
        private System.Windows.Forms.GroupBox grpGraphicControler;
        private System.Windows.Forms.GroupBox grpActivitiesSelection;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox grpFiles;
        private System.Windows.Forms.CheckedListBox cklLogFileSelection;
        private System.Windows.Forms.GroupBox grpMain;
        private ZedGraph.ZedGraphControl zg1;
        private System.Windows.Forms.CheckBox ckbADD;
        private System.Windows.Forms.CheckBox ckbREMOVE;
        private System.Windows.Forms.CheckBox ckbMOVE_FROM;
        private System.Windows.Forms.CheckBox ckbMOVE_TO;
        private System.Windows.Forms.CheckBox ckbSET;
        private System.Windows.Forms.CheckBox ckbCREATE;
        private PJLControls.ColorPicker clpkREMOVE;
        private PJLControls.ColorPicker clpkMOVEFROM;
        private PJLControls.ColorPicker clpkMOVETO;
        private PJLControls.ColorPicker clpkADD;
        private PJLControls.ColorPicker clpkCREATE;
        private PJLControls.ColorPicker clpkSET;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnLoadMultipleLogFile;
        private System.Windows.Forms.CheckBox ckbPACKAGE;
        private System.Windows.Forms.CheckBox ckbATTRIBUTE;
        private System.Windows.Forms.CheckBox ckbCLASS;
        private System.Windows.Forms.CheckBox ckbASSOCIATION;
        private System.Windows.Forms.CheckBox ckbOPERATION;
        private System.Windows.Forms.ComboBox cbbCLASS;
        private System.Windows.Forms.ComboBox cbbASSOCIATION;
        private System.Windows.Forms.ComboBox cbbATTRIBUTE;
        private System.Windows.Forms.ComboBox cbbOPERATION;
        private System.Windows.Forms.ComboBox cbbPACKAGE;
        private PJLControls.ColorPicker clpkPACKAGE;
        private System.Windows.Forms.CheckBox cbxAttrColoringMode;
        private PJLControls.ColorPicker clpkASSOCIATION;
        private PJLControls.ColorPicker clpkATTRIBUTE;
        private PJLControls.ColorPicker clpkOPERATION;
        private PJLControls.ColorPicker clpkCLASS;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvTimeMeasurement;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cl_Invisible;
        private System.Windows.Forms.DataGridViewImageColumn cl_Remove;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl_Points;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl_TimeMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl_X;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl_Y;
        private System.Windows.Forms.NumericUpDown sizePACKAGE;
        private System.Windows.Forms.NumericUpDown sizeASSOCIATION;
        private System.Windows.Forms.NumericUpDown sizeATTRIBUTE;
        private System.Windows.Forms.NumericUpDown sizeOPERARTION;
        private System.Windows.Forms.NumericUpDown sizeCLASS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHovering;
        private CustomizedToolTip ttDesignImage;
        private PJLControls.ColorPicker clpkBFREAD;
        private System.Windows.Forms.CheckBox ckbBFREAD;
        private PJLControls.ColorPicker clpkREAD;
        private System.Windows.Forms.CheckBox ckbREAD;
    }
}

