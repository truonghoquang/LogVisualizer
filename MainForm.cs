using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using System.Collections;
using System.IO;
using System.Drawing.Drawing2D;
using System.Linq;

namespace LogVisualizer
{
    public partial class frmUMLLogVisualizer : Form
    {
        private GraphPane m_graphPane;
        List<StudentLogging> students;
        List<string> logFileNames;
        List<Image> logCompletionDesigns;
        int nLogs;

        string strategies = "";

        private PointPairList plMOVETO_CLASS;
        private PointPairList plMOVETO_ASSOC;
        private PointPairList plMOVETO_OPERATION;
        private PointPairList plMOVETO_ATTR;
        private PointPairList plMOVETO_PACKAGE;

        // For action MOVE_FROM
        private PointPairList plMOVEFROM_CLASS;
        private PointPairList plMOVEFROM_ASSOC;
        private PointPairList plMOVEFROM_OPERATION;
        private PointPairList plMOVEFROM_ATTR;
        private PointPairList plMOVEFROM_PACKAGE;
        // For action CREATE
        private PointPairList plCREATE_CLASS;
        private PointPairList plCREATE_ASSOC;
        private PointPairList plCREATE_OPERATION;
        private PointPairList plCREATE_ATTR;
        private PointPairList plCREATE_PACKAGE;
        // For action ADD
        private PointPairList plADD_CLASS;
        private PointPairList plADD_ASSOC;
        private PointPairList plADD_OPERATION;
        private PointPairList plADD_ATTR;
        private PointPairList plADD_PACKAGE;
        // For action SET
        private PointPairList plSET_CLASS;
        private PointPairList plSET_ASSOC;
        private PointPairList plSET_OPERATION;
        private PointPairList plSET_ATTR;
        private PointPairList plSET_PACKAGE;
        // For action REMOVE
        private PointPairList plREMOVE_CLASS;
        private PointPairList plREMOVE_ASSOC;
        private PointPairList plREMOVE_OPERATION;
        private PointPairList plREMOVE_ATTR;
        private PointPairList plREMOVE_PACKAGE;

        private PointPairList plREADING;
        private PointPairList plBFREADING;

        private List<EmphasizedPoint> emphasizedCircles;

        public frmUMLLogVisualizer()
        {
            InitializeComponent();
            students = new List<StudentLogging>();
            logFileNames = new List<string>();
            logCompletionDesigns = new List<Image>();
            InitializePointList();
            m_graphPane = zg1.GraphPane;
            zg1.GraphPane.YAxis.ScaleFormatEvent += new Axis.ScaleFormatHandler(yAxis_ScaleFormatEvent);
            zg1.GraphPane.YAxis.Scale.FontSpec.Size = 5.0f;
            zg1.GraphPane.YAxis.Scale.MinorStep = 1.0;
            zg1.GraphPane.YAxis.Scale.MajorStep = 1.0;
            // Hiding the Zero Line - X Axis
            zg1.GraphPane.YAxis.MajorGrid.IsZeroLine = false;
            FillPaneBackground();

            cbbCLASS.DataSource = Enum.GetValues(typeof(SymbolType));
            cbbCLASS.SelectedIndex = 0;
            cbbOPERATION.DataSource = Enum.GetValues(typeof(SymbolType));
            cbbOPERATION.SelectedIndex = 3;
            cbbATTRIBUTE.DataSource = Enum.GetValues(typeof(SymbolType));
            cbbATTRIBUTE.SelectedIndex = 5;
            cbbASSOCIATION.DataSource = Enum.GetValues(typeof(SymbolType));
            cbbASSOCIATION.SelectedIndex = 8;
            cbbPACKAGE.DataSource = Enum.GetValues(typeof(SymbolType));
            cbbPACKAGE.SelectedIndex = 1;

            clpkADD.Color = Helper.CLR_ADD;
            clpkCREATE.Color = Helper.CLR_CREATE;
            clpkMOVETO.Color = Helper.CLR_MOVE_TO;
            clpkMOVEFROM.Color = Helper.CLR_MOVE_FROM;
            clpkSET.Color = Helper.CLR_SET;
            clpkREMOVE.Color = Helper.CLR_REMOVE;

            clpkCLASS.Color = Helper.CLR_CLASS;
            clpkOPERATION.Color = Helper.CLR_OPERATION;
            clpkATTRIBUTE.Color = Helper.CLR_ATTRIBUTE;
            clpkASSOCIATION.Color = Helper.CLR_ASSOCIATION;
            clpkPACKAGE.Color = Helper.CLR_PACKAGE;

            sizeCLASS.Value = (decimal)Math.Round(Helper.SIZE_SYB_CLASS, 1);
            sizeOPERARTION.Value = (decimal)Math.Round(Helper.SIZE_SYB_OPERATION, 1);
            sizeATTRIBUTE.Value = (decimal)Math.Round(Helper.SIZE_SYB_ATTR, 1);
            sizeASSOCIATION.Value = (decimal)Math.Round(Helper.SIZE_SYB_ASSOC, 1);  
            sizePACKAGE.Value = (decimal)Math.Round(Helper.SIZE_SYB_PACKAGE, 1);

            //TextObj label = new TextObj("some text", 0, 1);
            //label.Location.CoordinateFrame = CoordType.XChartFractionYScale;
            //label.Location.AlignH = AlignH.Left;
            //zg1.GraphPane.GraphObjList.Add(label);
        }

        private void UpdatePaneScale() 
        {
            if (nLogs > 0)
                Helper.YMax = nLogs * Helper.offset_LOG;
            Helper.YMin = -1.0;

            //zg1.GraphPane.XAxis.Scale.Max = Helper.XMax;
            zg1.GraphPane.XAxis.Scale.Min = Helper.XMin;
            zg1.GraphPane.YAxis.Scale.Max = Helper.YMax;
            zg1.GraphPane.YAxis.Scale.Min = Helper.YMin;
        }
        private void InitializePointList()
        {
            this.plMOVETO_CLASS = new PointPairList();
            this.plMOVETO_ASSOC = new PointPairList();
            this.plMOVETO_OPERATION = new PointPairList();
            this.plMOVETO_ATTR = new PointPairList();
            this.plMOVETO_PACKAGE = new PointPairList();
            // For action MOVE_FROM
            this.plMOVEFROM_CLASS = new PointPairList();
            this.plMOVEFROM_ASSOC = new PointPairList();
            this.plMOVEFROM_OPERATION = new PointPairList();
            this.plMOVEFROM_ATTR = new PointPairList();
            this.plMOVEFROM_PACKAGE = new PointPairList();
            // For action CREATE
            this.plCREATE_CLASS = new PointPairList();
            this.plCREATE_ASSOC = new PointPairList();
            this.plCREATE_OPERATION = new PointPairList();
            this.plCREATE_ATTR = new PointPairList();
            this.plCREATE_PACKAGE = new PointPairList();
            // For action ADD
            this.plADD_CLASS = new PointPairList();
            this.plADD_ASSOC = new PointPairList();
            this.plADD_OPERATION = new PointPairList();
            this.plADD_ATTR = new PointPairList();
            this.plADD_PACKAGE = new PointPairList();
            // For action SET
            this.plSET_CLASS = new PointPairList();
            this.plSET_ASSOC = new PointPairList();
            this.plSET_OPERATION = new PointPairList();
            this.plSET_ATTR = new PointPairList();
            this.plSET_PACKAGE = new PointPairList();
            // For action REMOVE
            this.plREMOVE_CLASS = new PointPairList();
            this.plREMOVE_ASSOC = new PointPairList();
            this.plREMOVE_OPERATION = new PointPairList();
            this.plREMOVE_ATTR = new PointPairList();
            this.plREMOVE_PACKAGE = new PointPairList();
            // For READING and MODELING
            this.plREADING = new PointPairList();
            this.plBFREADING = new PointPairList();

            emphasizedCircles = new List<EmphasizedPoint>();
        }

        private void SetLineBarTitleAndAxisDetails(ref string _graphTitle, ref string _xTitle, ref string _yTitle)
        {
            _graphTitle = txtPlotTitle.Text;
            string _xTitleTemp = txtXTitle.Text;
            string _yTitleTemp = txtYTitle.Text;
            string _xUnit = txtXUnit.Text;
            string _yUnit = txtYUnit.Text;

            if (_graphTitle == "")
            {
                _graphTitle = "Graph Test";
            }

            if (_xTitleTemp == "")
            {
                _xTitle = "X Axis";
            }

            if (_yTitleTemp == "")
            {
                _yTitle = "Y Axis";
            }

            if (_xUnit != "")
            {
                _xTitle = _xTitleTemp + " (" + _xUnit + " )";
            }

            if (_yUnit != "")
            {
                _yTitle = _yTitleTemp + " (" + _yUnit + " )";
            }
        }

        private void FillPaneBackground()
        {
            // Fill the axis background with a color gradient
            m_graphPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45F);
            // Fill the pane background with a color gradient
            m_graphPane.Fill = new Fill(Color.White, Color.FromArgb(220, 220, 255), 45F);
        }

        private void Redraw() 
        {
            //clear if anything exists.            
            m_graphPane.CurveList.Clear();
            string _graphTitle = "", _xTitle = "", _yTitle = "";

            // Set the titles and axis labels
            SetLineBarTitleAndAxisDetails(ref _graphTitle, ref _xTitle, ref _yTitle);
            m_graphPane.Title.Text = _graphTitle;
            m_graphPane.XAxis.Title.Text = _xTitle;
            m_graphPane.YAxis.Title.Text = _yTitle;

            // Generate activity diagrams for each student
            if (grpActivitiesSelection.Enabled)
            {
                for (int i = 0; i < nLogs; i++)
                {
                    if (this.ckbMOVE_TO.Checked)
                    {
                        if (ckbCLASS.Checked)
                            AddScatterPlot("plMOVETO_CLASS", plMOVETO_CLASS, Helper.CLR_MOVE_TO, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbASSOCIATION.Checked)
                            AddScatterPlot("plMOVETO_ASSOC", plMOVETO_ASSOC, Helper.CLR_MOVE_TO, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbOPERATION.Checked)
                            AddScatterPlot("plMOVETO_OPERATION", plMOVETO_OPERATION, Helper.CLR_MOVE_TO, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbATTRIBUTE.Checked)
                            AddScatterPlot("plMOVETO_ATTR", plMOVETO_ATTR, Helper.CLR_MOVE_TO, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbPACKAGE.Checked)
                            AddScatterPlot("plMOVETO_PACKAGE", plMOVETO_PACKAGE, Helper.CLR_MOVE_TO, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbMOVE_FROM.Checked)
                    {
                        if (ckbCLASS.Checked)
                            AddScatterPlot("plMOVEFROM_CLASS", plMOVEFROM_CLASS, Helper.CLR_MOVE_FROM, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbASSOCIATION.Checked)
                            AddScatterPlot("plMOVEFROM_ASSOC", plMOVEFROM_ASSOC, Helper.CLR_MOVE_FROM, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbOPERATION.Checked)
                            AddScatterPlot("plMOVEFROM_OPERATION", plMOVEFROM_OPERATION, Helper.CLR_MOVE_FROM, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbATTRIBUTE.Checked)
                            AddScatterPlot("plMOVEFROM_ATTR", plMOVEFROM_ATTR, Helper.CLR_MOVE_FROM, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbPACKAGE.Checked)
                            AddScatterPlot("plMOVEFROM_PACKAGE", plMOVEFROM_PACKAGE, Helper.CLR_MOVE_FROM, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbADD.Checked)
                    {
                        if (ckbCLASS.Checked)
                            AddScatterPlot("plADD_CLASS", plADD_CLASS, Helper.CLR_ADD, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbASSOCIATION.Checked)
                            AddScatterPlot("plADD_ASSOC", plADD_ASSOC, Helper.CLR_ADD, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbOPERATION.Checked)
                            AddScatterPlot("plADD_OPERATION", plADD_OPERATION, Helper.CLR_ADD, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbATTRIBUTE.Checked)
                            AddScatterPlot("plADD_ATTR", plADD_ATTR, Helper.CLR_ADD, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbPACKAGE.Checked)
                            AddScatterPlot("plADD_PACKAGE", plADD_PACKAGE, Helper.CLR_ADD, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbCREATE.Checked)
                    {
                        if (ckbCLASS.Checked)
                            AddScatterPlot("plCREATE_CLASS", plCREATE_CLASS, Helper.CLR_CREATE, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbASSOCIATION.Checked)
                            AddScatterPlot("plCREATE_ASSOC", plCREATE_ASSOC, Helper.CLR_CREATE, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbOPERATION.Checked)
                            AddScatterPlot("plCREATE_OPERATION", plCREATE_OPERATION, Helper.CLR_CREATE, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbATTRIBUTE.Checked)
                            AddScatterPlot("plCREATE_ATTR", plCREATE_ATTR, Helper.CLR_CREATE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbPACKAGE.Checked)
                            AddScatterPlot("plCREATE_PACKAGE", plCREATE_PACKAGE, Helper.CLR_CREATE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbSET.Checked)
                    {
                        if (ckbCLASS.Checked)
                            AddScatterPlot("plSET_CLASS", plSET_CLASS, Helper.CLR_SET, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbASSOCIATION.Checked)
                            AddScatterPlot("plSET_ASSOC", plSET_ASSOC, Helper.CLR_SET, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbOPERATION.Checked)
                            AddScatterPlot("plSET_OPERATION", plSET_OPERATION, Helper.CLR_SET, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbATTRIBUTE.Checked)
                            AddScatterPlot("plSET_ATTR", plSET_ATTR, Helper.CLR_SET, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbPACKAGE.Checked)
                            AddScatterPlot("plSET_PACKAGE", plSET_PACKAGE, Helper.CLR_SET, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbREMOVE.Checked)
                    {
                        if (ckbCLASS.Checked)
                            AddScatterPlot("plREMOVE_CLASS", plREMOVE_CLASS, Helper.CLR_REMOVE, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbASSOCIATION.Checked)
                            AddScatterPlot("plREMOVE_ASSOC", plREMOVE_ASSOC, Helper.CLR_REMOVE, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbOPERATION.Checked)
                            AddScatterPlot("plREMOVE_OPERATION", plREMOVE_OPERATION, Helper.CLR_REMOVE, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbATTRIBUTE.Checked)
                            AddScatterPlot("plREMOVE_ATTR", plREMOVE_ATTR, Helper.CLR_REMOVE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbPACKAGE.Checked)
                            AddScatterPlot("plREMOVE_PACKAGE", plREMOVE_PACKAGE, Helper.CLR_REMOVE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbREAD.Checked)
                        AddScatterPlot("plREAD", plREADING, Helper.CLR_READING, Helper.SYB_READING, Helper.SIZE_SYB_READING);
                    if (this.ckbBFREAD.Checked)
                        AddScatterPlot("plBFREAD", plBFREADING, Helper.CLR_BFREADING, Helper.SYB_BFREADING, Helper.SIZE_SYB_BFREADING);
                }
            }
            else
            {
                for (int i = 0; i < nLogs; i++)
                {
                    if (this.ckbCLASS.Checked)
                    {
                        if (ckbMOVE_TO.Checked)
                            AddScatterPlot("plMOVETO_CLASS", plMOVETO_CLASS, Helper.CLR_CLASS, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbMOVE_FROM.Checked)
                            AddScatterPlot("plMOVEFROM_CLASS", plMOVEFROM_CLASS, Helper.CLR_CLASS, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbADD.Checked)
                            AddScatterPlot("plADD_CLASS", plADD_CLASS, Helper.CLR_CLASS, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbCREATE.Checked)
                            AddScatterPlot("plCREATE_CLASS", plCREATE_CLASS, Helper.CLR_CLASS, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbSET.Checked)
                            AddScatterPlot("plSET_CLASS", plSET_CLASS, Helper.CLR_CLASS, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                        if (ckbREMOVE.Checked)
                            AddScatterPlot("plREMOVE_CLASS", plREMOVE_CLASS, Helper.CLR_CLASS, Helper.SYB_CLASS, Helper.SIZE_SYB_CLASS);
                    }
                    if (this.ckbOPERATION.Checked)
                    {
                        if (ckbMOVE_TO.Checked)
                            AddScatterPlot("plMOVETO_OPERATION", plMOVETO_OPERATION, Helper.CLR_OPERATION, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbMOVE_FROM.Checked)
                            AddScatterPlot("plMOVEFROM_OPERATION", plMOVEFROM_OPERATION, Helper.CLR_OPERATION, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbADD.Checked)
                            AddScatterPlot("plADD_OPERATION", plADD_OPERATION, Helper.CLR_OPERATION, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbCREATE.Checked)
                            AddScatterPlot("plCREATE_OPERATION", plCREATE_OPERATION, Helper.CLR_OPERATION, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbSET.Checked)
                            AddScatterPlot("plSET_OPERATION", plSET_OPERATION, Helper.CLR_OPERATION, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                        if (ckbREMOVE.Checked)
                            AddScatterPlot("plREMOVE_OPERATION", plREMOVE_OPERATION, Helper.CLR_OPERATION, Helper.SYB_OPERATION, Helper.SIZE_SYB_OPERATION);
                    }
                    if (this.ckbATTRIBUTE.Checked)
                    {
                        if (ckbMOVE_TO.Checked)
                            AddScatterPlot("plMOVETO_ATTR", plMOVETO_ATTR, Helper.CLR_ATTRIBUTE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbMOVE_FROM.Checked)
                            AddScatterPlot("plMOVEFROM_ATTR", plMOVEFROM_ATTR, Helper.CLR_ATTRIBUTE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbADD.Checked)
                            AddScatterPlot("plADD_ATTR", plADD_ATTR, Helper.CLR_ATTRIBUTE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbCREATE.Checked)
                            AddScatterPlot("plCREATE_ATTR", plCREATE_ATTR, Helper.CLR_ATTRIBUTE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbSET.Checked)
                            AddScatterPlot("plSET_ATTR", plSET_ATTR, Helper.CLR_ATTRIBUTE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                        if (ckbREMOVE.Checked)
                            AddScatterPlot("plREMOVE_ATTR", plREMOVE_ATTR, Helper.CLR_ATTRIBUTE, Helper.SYB_ATTR, Helper.SIZE_SYB_ATTR);
                    }
                    if (this.ckbASSOCIATION.Checked)
                    {
                        if (ckbMOVE_TO.Checked)
                            AddScatterPlot("plMOVETO_ASSOC", plMOVETO_ASSOC, Helper.CLR_ASSOCIATION, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbMOVE_FROM.Checked)
                            AddScatterPlot("plMOVEFROM_ASSOC", plMOVEFROM_ASSOC, Helper.CLR_ASSOCIATION, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbADD.Checked)
                            AddScatterPlot("plADD_ASSOC", plADD_ASSOC, Helper.CLR_ASSOCIATION, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbCREATE.Checked)
                            AddScatterPlot("plCREATE_ASSOC", plCREATE_ASSOC, Helper.CLR_ASSOCIATION, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbSET.Checked)
                            AddScatterPlot("plSET_ASSOC", plSET_ASSOC, Helper.CLR_ASSOCIATION, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                        if (ckbREMOVE.Checked)
                            AddScatterPlot("plREMOVE_ASSOC", plREMOVE_ASSOC, Helper.CLR_ASSOCIATION, Helper.SYB_ASSOC, Helper.SIZE_SYB_ASSOC);
                    }
                    if (this.ckbPACKAGE.Checked)
                    {
                        if (ckbMOVE_TO.Checked)
                            AddScatterPlot("plMOVETO_PACKAGE", plMOVETO_PACKAGE, Helper.CLR_PACKAGE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                        if (ckbMOVE_FROM.Checked)
                            AddScatterPlot("plMOVEFROM_PACKAGE", plMOVEFROM_PACKAGE, Helper.CLR_PACKAGE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                        if (ckbADD.Checked)
                            AddScatterPlot("plADD_PACKAGE", plADD_PACKAGE, Helper.CLR_PACKAGE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                        if (ckbCREATE.Checked)
                            AddScatterPlot("plCREATE_PACKAGE", plCREATE_PACKAGE, Helper.CLR_PACKAGE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                        if (ckbSET.Checked)
                            AddScatterPlot("plSET_PACKAGE", plSET_PACKAGE, Helper.CLR_PACKAGE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                        if (ckbREMOVE.Checked)
                            AddScatterPlot("plREMOVE_PACKAGE", plREMOVE_PACKAGE, Helper.CLR_PACKAGE, Helper.SYB_PACKAGE, Helper.SIZE_SYB_PACKAGE);
                    }
                    if (this.ckbREAD.Checked)
                        AddScatterPlot("plREAD", plREADING, Helper.CLR_READING, Helper.SYB_READING, Helper.SIZE_SYB_READING);
                    if (this.ckbBFREAD.Checked)
                        AddScatterPlot("plBFREAD", plBFREADING, Helper.CLR_BFREADING, Helper.SYB_BFREADING, Helper.SIZE_SYB_BFREADING);
                }
            }
            
            // emphasized points
            if (emphasizedCircles.Count > 0)
            {
                for (int i = 0; i < emphasizedCircles.Count; i++)
                {
                    if (emphasizedCircles[i].Visible)
                        zg1.GraphPane.CurveList.Add(emphasizedCircles[i].Line);
                }
            }
            
            // Calculate the Axis Scale Ranges
            zg1.GraphPane.Legend.IsVisible = false;
            zg1.AxisChange();
            zg1.Refresh();

        }
        private void AddScatterPlot(string _label, PointPairList list, Color _color, SymbolType _symbol, float _symbolSize)
        {
            // Add the curve
            LineItem myCurve = m_graphPane.AddCurve("", list, _color, _symbol);
            myCurve.Symbol.Size = _symbolSize;
            // Don't display the line (This makes a scatter plot)
            myCurve.Line.IsVisible = false;

            if (_symbol == SymbolType.UserDefined)
            {
                myCurve.Symbol = new Symbol(SymbolType.UserDefined, _color);
                myCurve.Symbol.UserSymbol = Helper.SYB_USERDEFINED;
            }
            //// Hide the symbol outline
            //myCurve.Symbol.Border.IsVisible = false;
            // Fill the symbol interior with color
            myCurve.Symbol.Fill = new Fill(_color);


            //// Fill the background of the chart rect and pane
            //m_graphPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45.0f);
            //m_graphPane.Fill = new Fill(Color.White, Color.SlateGray, 45.0f);
        }
   
        private void AddSingleLog(StudentLogging log) 
        {
            if (students == null)
            {
                students = new List<StudentLogging>();
                InitializePointList();
            }
            students.Add(log);
            // Update Point List
            plMOVETO_CLASS.AddRange(log.PlMOVETO_CLASS);
            plMOVETO_ASSOC.AddRange(log.PlMOVETO_ASSOC);
            plMOVETO_OPERATION.AddRange(log.PlMOVETO_OPERATION);
            plMOVETO_ATTR.AddRange(log.PlMOVETO_ATTR);
            plMOVETO_PACKAGE.AddRange(log.PlCREATE_PACKAGE);
            // For action MOVE_FROM
            plMOVEFROM_CLASS.AddRange(log.PlMOVEFROM_CLASS);
            plMOVEFROM_ASSOC.AddRange(log.PlMOVEFROM_ASSOC);
            plMOVEFROM_OPERATION.AddRange(log.PlMOVEFROM_OPERATION);
            plMOVEFROM_ATTR.AddRange(log.PlMOVEFROM_ATTR);
            plMOVEFROM_PACKAGE.AddRange(log.PlCREATE_PACKAGE);
            // For action CREATE
            plCREATE_CLASS.AddRange(log.PlCREATE_CLASS);
            plCREATE_ASSOC.AddRange(log.PlCREATE_ASSOC);
            plCREATE_OPERATION.AddRange(log.PlCREATE_OPERATION);
            plCREATE_ATTR.AddRange(log.PlCREATE_ATTR);
            plCREATE_PACKAGE.AddRange(log.PlCREATE_PACKAGE);
            // For action ADD
            plADD_CLASS.AddRange(log.PlADD_CLASS);
            plADD_ASSOC.AddRange(log.PlADD_ASSOC);
            plADD_OPERATION.AddRange(log.PlADD_OPERATION);
            plADD_ATTR.AddRange(log.PlADD_ATTR);
            plADD_ATTR.AddRange(log.PlADD_PACKAGE);
            // For action SET
            plSET_CLASS.AddRange(log.PlSET_CLASS);
            plSET_ASSOC.AddRange(log.PlSET_ASSOC);
            plSET_OPERATION.AddRange(log.PlSET_OPERATION);
            plSET_ATTR.AddRange(log.PlSET_ATTR);
            plSET_ATTR.AddRange(log.PlSET_PACKAGE);
            // For action REMOVE
            plREMOVE_CLASS.AddRange(log.PlSET_CLASS);
            plREMOVE_ASSOC.AddRange(log.PlSET_ASSOC);
            plREMOVE_OPERATION.AddRange(log.PlSET_OPERATION);
            plREMOVE_ATTR.AddRange(log.PlSET_ATTR);
            plREMOVE_ATTR.AddRange(log.PlSET_PACKAGE);
            // For action READING
            plREADING.AddRange(log.PlREADING);
            // For action Back From Reading (BFREADING)
            plBFREADING.AddRange(log.PlBFREADING);
            Redraw();
        }

        private void btnLoadFromFolder_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                // reset logs list
                students = new List<StudentLogging>();
                logFileNames = new List<string>();
                //logCompletionDesigns = new List<Image>();
                cklLogFileSelection.Items.Clear();

                String[] filePaths = Directory.GetFiles(dialog.SelectedPath, "*.csv", SearchOption.AllDirectories);
                for (int i = 0; i < filePaths.Length; i++)
                {
                    if (AddSingleLogFile(filePaths[i], i))
                    {
                        this.nLogs++;
                        //logFileNames.Add(Path.GetFileNameWithoutExtension(filePaths[i]).Replace("-cleaned", ""));
                        logFileNames.Add(Path.GetFullPath(filePaths[i]));
                        //logCompletionDesigns.Add(Image.FromFile(filePaths[i].Replace(".csv", ".png")));
                        cklLogFileSelection.Items.Add(filePaths[i]);
                    }
                }
                // save strategies into file
                File.WriteAllText(Path.Combine(dialog.SelectedPath, Helper.STRATEGY_FILE_NAME), strategies);
            }
            if (cklLogFileSelection.Items != null && cklLogFileSelection.Items.Count > 0)
            {
                for (int j = 0; j < cklLogFileSelection.Items.Count; j++)
                {
                    cklLogFileSelection.SetItemChecked(j, true);
                }
            }
            UpdatePaneScale();
            Redraw();
        }

        private void btnLoadMultipleLogFiles_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            DialogResult result = dialog.ShowDialog();

            int seq = this.nLogs + 1;
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string[] filePaths = dialog.FileNames;
                for (int i = 0; i < filePaths.Length; i++)
                {
                    if (AddSingleLogFile(filePaths[i], i))
                    {
                        this.nLogs++;
                        logFileNames.Add(Path.GetFileNameWithoutExtension(filePaths[i]).Replace("-cleaned", ""));
                        Image img = null;
                        try
                        {
                            img = Image.FromFile(filePaths[i].Replace(".csv", ".png"));

                        }
                        catch (Exception)
                        {
                            //img = 
                        }

                        //logCompletionDesigns.Add(img);
                        cklLogFileSelection.Items.Add(filePaths[i]);
                    }
                }
                if (cklLogFileSelection.Items != null && cklLogFileSelection.Items.Count > 0)
                {
                    for (int j = 0; j < cklLogFileSelection.Items.Count; j++)
                    {
                        cklLogFileSelection.SetItemChecked(j, true);
                    }
                }
                UpdatePaneScale();
                Redraw();
            }  
        }

        private bool AddSingleLogFile(String _filePath, int _intSequenceNumber)
        {
            if (_filePath != "")
            {
                // sort the file by timestamp (ASCENDING)
                string[] lines = File.ReadAllLines(_filePath);
                var sorted = lines.Select(line => new
                {
                    SortKey = line.Split(',')[0],
                    Line = line
                })
                            .OrderBy(x => x.SortKey)
                            .Select(x => x.Line);

                // lines is sorted
                List<Double> lstTime = new List<Double>();
                List<string> lstActions = new List<string>();
                List<string> lstObjects = new List<string>();
                List<string> lstInfor = new List<string>();
                Double minValue = Double.MaxValue;
                foreach (var line in sorted.ToArray())
                {
                    var values = line.Split(',');
                    String time = "";

                    if (values[0].StartsWith("undefined"))
                        time = values[0].Replace("undefined", "");
                    else
                        time = values[0];
                    Double _timeToAdd = 0;
                    if (Double.TryParse(time, out _timeToAdd))
                    {
                        if (minValue > _timeToAdd)
                            minValue = _timeToAdd;
                        lstTime.Add(_timeToAdd);
                        lstActions.Add(values[1]);
                        if (values.Length > 2)
                            lstObjects.Add(values[2]);
                        else
                            lstObjects.Add("");
                        lstInfor.Add(line);
                    }     
                }

                if (lstTime != null && lstTime.Count >= 1)
                {
                    string strategy = _filePath + ", ";
                    // Get time-gap between two sequence activities
                    for (int i = 0; i < lstTime.Count; i++)
                    {
                        lstTime[i] = (lstTime[i] - minValue) / 1000.0;
                        //Double timeGap = i > 0 ? (lstTime[i] - lstTime[i - 1]) : lstTime[i];
                        PointPair pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG, lstInfor[i]);
                        switch (lstActions[i].ToUpper())
                        {
                            case "MOVE TO":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_MOVE_TO, lstInfor[i]);
                                break;
                            case "MOVE FROM":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_MOVE_FROM, lstInfor[i]);
                                break;
                            case "CREATE":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_CREATE, lstInfor[i]);
                                break;
                            case "ADD":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_ADD, lstInfor[i]);
                                break;
                            case "SET":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_SET, lstInfor[i]);
                                break;
                            case "REMOVE":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_REMOVE, lstInfor[i]);
                                break;
                            case "READING":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_READING, "READING ASSIGNMENT TEXT");
                                break;
                            case "MODELING":
                                pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_BFREADING, "BACK FROM READING ASSIGNMENT TEXT");
                                break;
                        }
                        //switch (lstObjects[i].ToUpper())
                        //{
                        //    case "CLASS":
                        //        pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_CLASS, lstInfor[i]);
                        //        break;
                        //    case "OPERATION":
                        //        pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_OPERATION, lstInfor[i]);
                        //        break;
                        //    case "ATTRIBUTE":
                        //        pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_ATTRIBUTE, lstInfor[i]);
                        //        break;
                        //    case "ASSOCIATION":
                        //        pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_ASSOCIATION, lstInfor[i]);
                        //        break;
                        //    case "PACKAGE":
                        //        pointToAdd = new PointPair(lstTime[i], _intSequenceNumber * Helper.offset_LOG + Helper.offset_PACKAGE, lstInfor[i]);
                        //        break;
                        //}
                        // Add points to lists
                        switch (lstActions[i].ToUpper())
                        {
                            case "MOVE TO":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plMOVETO_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plMOVETO_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plMOVETO_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plMOVETO_ATTR.Add(pointToAdd);
                                        break;
                                    case "PACKAGE":
                                        this.plMOVETO_PACKAGE.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "MOVE FROM":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plMOVEFROM_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plMOVEFROM_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plMOVEFROM_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plMOVEFROM_ATTR.Add(pointToAdd);
                                        break;
                                    case "PACKAGE":
                                        this.plMOVEFROM_PACKAGE.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "CREATE":
                                //if (timeGap > 200)
                                //    strategy += "T";
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plCREATE_CLASS.Add(pointToAdd);
                                        strategy += "C";
                                        break;
                                    case "ASSOCIATION":
                                        this.plCREATE_ASSOC.Add(pointToAdd);
                                        strategy += "R";
                                        break;
                                    case "OPERATION":
                                        this.plCREATE_OPERATION.Add(pointToAdd);
                                        strategy += "O";
                                        break;
                                    case "ATTRIBUTE":
                                        this.plCREATE_ATTR.Add(pointToAdd);
                                        strategy += "A";
                                        break;
                                    case "PACKAGE":
                                        this.plCREATE_PACKAGE.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "ADD":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plADD_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plADD_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plADD_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plADD_ATTR.Add(pointToAdd);
                                        break;
                                    case "PACKAGE":
                                        this.plADD_PACKAGE.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "SET":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plSET_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plSET_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plSET_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plSET_ATTR.Add(pointToAdd);
                                        break;
                                    case "PACKAGE":
                                        this.plSET_PACKAGE.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "REMOVE":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plREMOVE_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plREMOVE_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plREMOVE_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plREMOVE_ATTR.Add(pointToAdd);
                                        break;
                                    case "PACKAGE":
                                        this.plREMOVE_PACKAGE.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "READING":
                                this.plREADING.Add(pointToAdd);
                                break;
                            case "MODELING":
                                this.plBFREADING.Add(pointToAdd);
                                break;
                        } 
                    }
                    strategies += strategy + "\n";
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ckbCREATE_CheckedChanged(object sender, EventArgs e)
        {
            clpkCREATE.Enabled = ckbCREATE.Checked;
            Redraw();
        }

        private void ckbADD_CheckedChanged(object sender, EventArgs e)
        {
            clpkADD.Enabled = ckbADD.Checked;
            Redraw();
        }

        private void ckbSET_CheckedChanged(object sender, EventArgs e)
        {
            clpkSET.Enabled = ckbSET.Checked;
            Redraw();
        }

        private void ckbMOVE_FROM_CheckedChanged(object sender, EventArgs e)
        {
            clpkMOVEFROM.Enabled = ckbMOVE_FROM.Checked;
            Redraw();
        }

        private void ckbREMOVE_CheckedChanged(object sender, EventArgs e)
        {
            clpkREMOVE.Enabled = ckbREMOVE.Checked;
            Redraw();
        }

        private void ckbMOVE_TO_CheckedChanged(object sender, EventArgs e)
        {
            clpkMOVETO.Enabled = ckbMOVE_TO.Checked;
            Redraw();
        }

        private void clpkSET_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_SET = clpkSET.Color;
            Redraw();
        }

        private void clpkCREATE_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_CREATE = clpkCREATE.Color;
            Redraw();
        }

        private void clpkADD_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_ADD = clpkADD.Color;
            Redraw();
        }

        private void clpkMOVETO_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_MOVE_TO = clpkMOVETO.Color;
            Redraw();
        }

        private void clpkMOVEFROM_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_MOVE_FROM = clpkMOVEFROM.Color;
            Redraw();
        }

        private void clpkREMOVE_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_REMOVE = clpkREMOVE.Color;
            Redraw();
        }

        private void clpkREAD_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_READING = clpkREAD.Color;
            Redraw();
        }

        private void clpkBFREAD_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_BFREADING = clpkBFREAD.Color;
            Redraw();
        }

        private void btnRedraw_Click(object sender, EventArgs e)
        {
            Redraw();
        }

        private void ckbCLASS_CheckedChanged(object sender, EventArgs e)
        {
            cbbCLASS.Enabled = ckbCLASS.Checked;
            sizeCLASS.Enabled = ckbCLASS.Checked;
            Redraw();
        }

        private void ckbOPERATION_CheckedChanged(object sender, EventArgs e)
        {
            cbbOPERATION.Enabled = ckbOPERATION.Checked;
            sizeOPERARTION.Enabled = ckbOPERATION.Checked;
            Redraw();
        }

        private void ckbATTRIBUTE_CheckedChanged(object sender, EventArgs e)
        {
            cbbATTRIBUTE.Enabled = ckbATTRIBUTE.Checked;
            sizeATTRIBUTE.Enabled = ckbATTRIBUTE.Checked;
            Redraw();
        }

        private void ckbASSOCIATION_CheckedChanged(object sender, EventArgs e)
        {
            cbbASSOCIATION.Enabled = ckbASSOCIATION.Checked;
            sizeASSOCIATION.Enabled = ckbASSOCIATION.Checked;
            Redraw();
        }

        private void ckbPACKAGE_CheckedChanged(object sender, EventArgs e)
        {
            cbbPACKAGE.Enabled = ckbPACKAGE.Checked;
            sizePACKAGE.Enabled = ckbPACKAGE.Checked;
            Redraw();
        }

        private void cbbCLASS_SelectedIndexChanged(object sender, EventArgs e)
        {
            SymbolType symbol = (SymbolType)(Enum.Parse(typeof(SymbolType), cbbCLASS.SelectedValue.ToString(), true));
            Helper.SYB_CLASS = symbol;
            Redraw();
        }

        private void cbbOPERATION_SelectedIndexChanged(object sender, EventArgs e)
        {
            SymbolType symbol = (SymbolType)(Enum.Parse(typeof(SymbolType), cbbOPERATION.SelectedValue.ToString(), true));
            Helper.SYB_OPERATION = symbol;
            Redraw();
        }

        private void cbbATTRIBUTE_SelectedIndexChanged(object sender, EventArgs e)
        {
            SymbolType symbol = (SymbolType)(Enum.Parse(typeof(SymbolType), cbbATTRIBUTE.SelectedValue.ToString(), true));
            Helper.SYB_ATTR = symbol;
            Redraw();
        }

        private void cbbASSOCIATION_SelectedIndexChanged(object sender, EventArgs e)
        {
            SymbolType symbol = (SymbolType)(Enum.Parse(typeof(SymbolType), cbbASSOCIATION.SelectedValue.ToString(), true));
            Helper.SYB_ASSOC = symbol;
            Redraw();
        }

        private void cbbPACKAGE_SelectedIndexChanged(object sender, EventArgs e)
        {
            SymbolType symbol = (SymbolType)(Enum.Parse(typeof(SymbolType), cbbPACKAGE.SelectedValue.ToString(), true));
            Helper.SYB_PACKAGE = symbol;
            Redraw();
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            students = new List<StudentLogging>();
            logFileNames = new List<string>();
            cklLogFileSelection.Items.Clear();
            InitializePointList();

            this.nLogs = 0;
            Redraw();
        }

        private void cbxAttrColoringMode_CheckedChanged(object sender, EventArgs e)
        {
            grpActivitiesSelection.Enabled = !cbxAttrColoringMode.Checked;
            clpkCLASS.Enabled = cbxAttrColoringMode.Checked;
            clpkOPERATION.Enabled = cbxAttrColoringMode.Checked;
            clpkATTRIBUTE.Enabled = cbxAttrColoringMode.Checked;
            clpkASSOCIATION.Enabled = cbxAttrColoringMode.Checked;
            //clpkPACKAGE.Enabled = cbxAttrColoringMode.Checked;
            Redraw();
        }

        private void clpkCLASS_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_CLASS = clpkCLASS.Color;
            Redraw();
        }

        private void clpkOPERATION_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_OPERATION = clpkOPERATION.Color;
            Redraw();
        }

        private void clpkATTRIBUTE_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_ATTRIBUTE = clpkATTRIBUTE.Color;
            Redraw();
        }

        private void clpkASSOCIATION_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_ASSOCIATION = clpkASSOCIATION.Color;
            Redraw();
        }

        private void clpkPACKAGE_ColorChanged(object sender, PJLControls.ColorChangedEventArgs e)
        {
            Helper.CLR_PACKAGE = clpkPACKAGE.Color;
            Redraw();
        }

        private bool zg1_MouseDownEvent(ZedGraphControl control, MouseEventArgs e)
        {
            if (Control.ModifierKeys == Keys.Alt)
            {
                object nearestObject;
                int index;
                this.zg1.GraphPane.FindNearestObject(new PointF(e.X, e.Y), this.CreateGraphics(), out nearestObject, out index);
                if (nearestObject != null && nearestObject is LineItem)
                {
                    LineItem lItem = (LineItem)nearestObject;
                    string pText = lItem[index].Tag.ToString();

                    double timeAfter = lItem[index].X;
                    double colY = lItem[index].Y;
                    double timeMeasurement = 0;
                    if (dgvTimeMeasurement.Rows.Count <= 0)
                    {
                        timeMeasurement = 0;
                    }
                    else
                    {
                        double timeBefore = (double)dgvTimeMeasurement.Rows[dgvTimeMeasurement.Rows.Count -1].Cells["cl_X"].Value;
                        timeMeasurement = timeAfter - timeBefore;
                    }

                    // Emphasizing circle
                    emphasizedCircles.Add(new EmphasizedPoint(timeAfter, colY, Helper.CLR_EMPHASIS, 
                        Helper.SYB_EMPHASIS, Helper.SIZE_EMPHASIS, Helper.LINEWIDTH_EMPHASIS));

                    dgvTimeMeasurement.Rows.Add(
                        true // cl_Invisible
                        , null // cl_Remove
                        , pText // cl_Points
                        , timeMeasurement // cl_TimeMeasurement
                        , timeAfter // cl_X
                        , colY  // cl_Y
                        );

                    // Resize
                    for (int i = 0; i < this.dgvTimeMeasurement.Rows.Count; i++)
                    {
                        this.dgvTimeMeasurement.AutoResizeRow(i);
                    }
                    Redraw();
                }
            }
            else if (Control.ModifierKeys == Keys.Control)
            {
                // Show completion design
                // Save the mouse location
                PointF mousePt = new PointF(e.X, e.Y);
                string tooltip = string.Empty;
                // Find the Chart rect that contains the current mouse location
                GraphPane pane = control.MasterPane.FindChartRect(mousePt);
                // If pane is non-null, we have a valid location.  Otherwise, the mouse is not
                // within any chart rect.
                if (pane != null)
                {
                    double x, y;
                    // Convert the mouse location to X, and Y scale values
                    pane.ReverseTransform(mousePt, out x, out y);
                    //  
                    txtHovering.Text = y.ToString();
                    
                }
            }
            return false;
        }

        private void dgvTimeMeasurement_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dgvTimeMeasurement.Rows.Count)
            {
                if (e.ColumnIndex == 1)
                {
                    dgvTimeMeasurement.Rows.RemoveAt(e.RowIndex);
                    emphasizedCircles.RemoveAt(e.RowIndex);
                    RecalculateTimeMeasurement(e.RowIndex);
                    Redraw();
                }
            }
        }

        private void RecalculateTimeMeasurement(int startingRow)
        {
            if (startingRow < dgvTimeMeasurement.Rows.Count)
            {
                if (startingRow == 0)
                {
                    dgvTimeMeasurement.Rows[startingRow].Cells["cl_TimeMeasurement"].Value = 0;
                }
                else if (startingRow > 0)
                {
                    dgvTimeMeasurement.Rows[startingRow].Cells["cl_TimeMeasurement"].Value =
                          (double)dgvTimeMeasurement.Rows[startingRow].Cells["cl_X"].Value
                          - (double)dgvTimeMeasurement.Rows[startingRow - 1].Cells["cl_X"].Value;
                }
            }
        }

        private void dgvTimeMeasurement_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvTimeMeasurement.IsCurrentCellDirty)
            {
                dgvTimeMeasurement.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgvTimeMeasurement_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dgvTimeMeasurement.Rows.Count)
            {
                if (e.ColumnIndex == 0)
                {
                    if ((bool)dgvTimeMeasurement.Rows[e.RowIndex].Cells[0].Value)
                    {
                        emphasizedCircles[e.RowIndex].Visible = true;
                    }
                    else
                    {
                        emphasizedCircles[e.RowIndex].Visible = false;
                    }
                    Redraw();
                }
            }
        }

        private string yAxis_ScaleFormatEvent(GraphPane pane, Axis axis, double val, int index)
        {
            if ((val >= 0) && ((val % 1) == 0) && logFileNames != null && logFileNames.Count > 0 && val < logFileNames.Count)
            {
                string path = logFileNames[(int)val];
                return path.Substring(path.Length > 10 ? path.Length - 10 : 0);
            }
            else return "";
        }

        private void sizeCLASS_ValueChanged(object sender, EventArgs e)
        {
            Helper.SIZE_SYB_CLASS = (float)sizeCLASS.Value;
            Redraw();
        }

        private void sizeOPERARTION_ValueChanged(object sender, EventArgs e)
        {
            Helper.SIZE_SYB_OPERATION = (float)sizeOPERARTION.Value;
            Redraw();
        }

        private void sizeATTRIBUTE_ValueChanged(object sender, EventArgs e)
        {
            Helper.SIZE_SYB_ATTR = (float)sizeATTRIBUTE.Value;
            Redraw();
        }

        private void sizeASSOCIATION_ValueChanged(object sender, EventArgs e)
        {
            Helper.SIZE_SYB_ASSOC = (float)sizeASSOCIATION.Value;
            Redraw();
        }

        private void sizePACKAGE_ValueChanged(object sender, EventArgs e)
        {
            Helper.SIZE_SYB_PACKAGE = (float)sizePACKAGE.Value;
            Redraw();
        }

        private void ckbREAD_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void ckbBFREAD_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }
    }
}