﻿using System;
using System.Collections.Generic;
using System.Text;
using ZedGraph;
using System.IO;

namespace LogVisualizer
{
    public class StudentLogging
    {
        public StudentLogging(String _filePath, int _sequenceNumber) 
        {
            this.filePath = _filePath;
            this.strName = Path.GetFileNameWithoutExtension(_filePath);
            this.intSequenceNumber = _sequenceNumber;
            this.loadFromFile();
        }

        #region Attributes
        private int intSequenceNumber = 0;
        private String strName = "";
        private String filePath = "";
        // For action MOVE_TO
        private PointPairList plMOVETO_CLASS = new PointPairList();
        private PointPairList plMOVETO_ASSOC = new PointPairList();
        private PointPairList plMOVETO_OPERATION = new PointPairList();
        private PointPairList plMOVETO_ATTR = new PointPairList();
        private PointPairList plMOVETO_PACKAGE = new PointPairList();
        // For action MOVE_FROM
        private PointPairList plMOVEFROM_CLASS = new PointPairList();
        private PointPairList plMOVEFROM_ASSOC = new PointPairList();
        private PointPairList plMOVEFROM_OPERATION = new PointPairList();
        private PointPairList plMOVEFROM_ATTR = new PointPairList();
        private PointPairList plMOVEFROM_PACKAGE = new PointPairList();
        // For action CREATE
        private PointPairList plCREATE_CLASS = new PointPairList();
        private PointPairList plCREATE_ASSOC = new PointPairList();
        private PointPairList plCREATE_OPERATION = new PointPairList();
        private PointPairList plCREATE_ATTR = new PointPairList();
        private PointPairList plCREATE_PACKAGE = new PointPairList();
        // For action ADD
        private PointPairList plADD_CLASS = new PointPairList();
        private PointPairList plADD_ASSOC = new PointPairList();
        private PointPairList plADD_OPERATION = new PointPairList();
        private PointPairList plADD_ATTR = new PointPairList();
        private PointPairList plADD_PACKAGE = new PointPairList();
        // For action SET
        private PointPairList plSET_CLASS = new PointPairList();
        private PointPairList plSET_ASSOC = new PointPairList();
        private PointPairList plSET_OPERATION = new PointPairList();
        private PointPairList plSET_ATTR = new PointPairList();
        private PointPairList plSET_PACKAGE = new PointPairList();
        // For action REMOVE
        private PointPairList plREMOVE_CLASS = new PointPairList();
        private PointPairList plREMOVE_ASSOC = new PointPairList(); 
        private PointPairList plREMOVE_OPERATION = new PointPairList();
        private PointPairList plREMOVE_ATTR = new PointPairList();
        private PointPairList plREMOVE_PACKAGE = new PointPairList();

        private PointPairList plREADING = new PointPairList();
        private PointPairList plBFREADING = new PointPairList();
        #endregion Attributes

        #region Getter,setter
        // MOVE_TO
        public PointPairList PlMOVETO_CLASS
        {
            get { return plMOVETO_CLASS; }
            set { plMOVETO_CLASS = value; }
        }
        public PointPairList PlMOVETO_ASSOC
        {
            get { return plMOVETO_ASSOC; }
            set { plMOVETO_ASSOC = value; }
        }
        public PointPairList PlMOVETO_OPERATION
        {
            get { return plMOVETO_OPERATION; }
            set { plMOVETO_OPERATION = value; }
        }
        public PointPairList PlMOVETO_ATTR
        {
            get { return PlMOVETO_ATTR; }
            set { PlMOVETO_ATTR = value; }
        }

        // MOVE_FROM
        public PointPairList PlMOVEFROM_CLASS
        {
            get { return plMOVEFROM_CLASS; }
            set { plMOVEFROM_CLASS = value; }
        }
        public PointPairList PlMOVEFROM_ASSOC
        {
            get { return plMOVEFROM_ASSOC; }
            set { plMOVEFROM_ASSOC = value; }
        }
        public PointPairList PlMOVEFROM_OPERATION
        {
            get { return plMOVEFROM_OPERATION; }
            set { plMOVEFROM_OPERATION = value; }
        }
        public PointPairList PlMOVEFROM_ATTR
        {
            get { return PlMOVEFROM_ATTR; }
            set { PlMOVEFROM_ATTR = value; }
        }

        // CREATE
        public PointPairList PlCREATE_CLASS
        {
            get { return plCREATE_CLASS; }
            set { plCREATE_CLASS = value; }
        }
        public PointPairList PlCREATE_ASSOC
        {
            get { return plCREATE_ASSOC; }
            set { plCREATE_ASSOC = value; }
        }
        public PointPairList PlCREATE_OPERATION
        {
            get { return plCREATE_OPERATION; }
            set { plCREATE_OPERATION = value; }
        }
        public PointPairList PlCREATE_ATTR
        {
            get { return PlCREATE_ATTR; }
            set { PlCREATE_ATTR = value; }
        }

        // ADD
        public PointPairList PlADD_CLASS
        {
            get { return plADD_CLASS; }
            set { plADD_CLASS = value; }
        }
        public PointPairList PlADD_ASSOC
        {
            get { return plADD_ASSOC; }
            set { plADD_ASSOC = value; }
        }
        public PointPairList PlADD_OPERATION
        {
            get { return plADD_OPERATION; }
            set { plADD_OPERATION = value; }
        }
        public PointPairList PlADD_ATTR
        {
            get { return PlADD_ATTR; }
            set { PlADD_ATTR = value; }
        }

        // SET
        public PointPairList PlSET_CLASS
        {
            get { return plSET_CLASS; }
            set { plSET_CLASS = value; }
        }
        public PointPairList PlSET_ASSOC
        {
            get { return plSET_ASSOC; }
            set { plSET_ASSOC = value; }
        }
        public PointPairList PlSET_OPERATION
        {
            get { return plSET_OPERATION; }
            set { plSET_OPERATION = value; }
        }
        public PointPairList PlSET_ATTR
        {
            get { return PlSET_ATTR; }
            set { PlSET_ATTR = value; }
        }
        // REMOVE
        public PointPairList PlREMOVE_CLASS
        {
            get { return plREMOVE_CLASS; }
            set { plREMOVE_CLASS = value; }
        }
        public PointPairList PlREMOVE_ASSOC
        {
            get { return plREMOVE_ASSOC; }
            set { plREMOVE_ASSOC = value; }
        }
        public PointPairList PlREMOVE_OPERATION
        {
            get { return plREMOVE_OPERATION; }
            set { plREMOVE_OPERATION = value; }
        }
        public PointPairList PlREMOVE_ATTR
        {
            get { return plREMOVE_ATTR; }
            set { plREMOVE_ATTR = value; }
        }
        public PointPairList PlMOVETO_PACKAGE
        {
            get { return plMOVETO_PACKAGE; }
            set { plMOVETO_PACKAGE = value; }
        }
        public PointPairList PlMOVEFROM_PACKAGE
        {
            get { return plMOVEFROM_PACKAGE; }
            set { plMOVEFROM_PACKAGE = value; }
        }
        public PointPairList PlCREATE_PACKAGE
        {
            get { return plCREATE_PACKAGE; }
            set { plCREATE_PACKAGE = value; }
        }
        public PointPairList PlADD_PACKAGE
        {
            get { return plADD_PACKAGE; }
            set { plADD_PACKAGE = value; }
        }

        public PointPairList PlREADING
        {
            get { return plREADING; }
            set { plREADING = value; }
        }
        public PointPairList PlBFREADING
        {
            get { return plBFREADING; }
            set { plBFREADING = value; }
        }

        public PointPairList PlSET_PACKAGE
        {
            get { return plSET_PACKAGE; }
            set { plSET_PACKAGE = value; }
        }
        public PointPairList PlREMOVE_PACKAGE
        {
            get { return plREMOVE_PACKAGE; }
            set { plREMOVE_PACKAGE = value; }
        }
        #endregion Getters,setters

        #region Functions 
        private bool loadFromFile()
        {
            if (this.filePath != "")
            {
                var reader = new StreamReader(File.OpenRead(this.filePath));
                List<Double> lstTime = new List<Double>();
                List<string> lstActions = new List<string>();
                List<string> lstObjects = new List<string>();
                Double minValue = Double.MaxValue;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    Double _timeToAdd = Convert.ToInt64(values[0]);
                    if (minValue > _timeToAdd)
                        minValue = _timeToAdd;
                    lstTime.Add(_timeToAdd);
                    lstActions.Add(values[1]);
                    lstObjects.Add(values[2]);
                }

                if (lstTime != null && lstTime.Count>=1)
                {
                    // Get time-gap between two sequence activities
                    for (int i = 0; i < lstTime.Count; i++)
                    {
                        lstTime[i] = (lstTime[i] - minValue) / 1000.0;
                        PointPair pointToAdd = new PointPair(lstTime[i], intSequenceNumber * Helper.offset_LOG);
                        // Add points to lists
                        switch (lstActions[i].ToUpper())
                        {
                            case "MOVE TO":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plMOVETO_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plMOVETO_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plMOVETO_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plMOVETO_ATTR.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "MOVE FROM":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plMOVEFROM_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plMOVEFROM_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plMOVEFROM_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plMOVEFROM_ATTR.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "CREATE":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plCREATE_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plCREATE_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plCREATE_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plCREATE_ATTR.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "ADD":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plADD_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plADD_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plADD_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plADD_ATTR.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "SET":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plSET_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plSET_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plSET_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plSET_ATTR.Add(pointToAdd);
                                        break;
                                }
                                break;
                            case "REMOVE":
                                switch (lstObjects[i].ToUpper())
                                {
                                    case "CLASS":
                                        this.plREMOVE_CLASS.Add(pointToAdd);
                                        break;
                                    case "ASSOCIATION":
                                        this.plREMOVE_ASSOC.Add(pointToAdd);
                                        break;
                                    case "OPERATION":
                                        this.plREMOVE_OPERATION.Add(pointToAdd);
                                        break;
                                    case "ATTRIBUTE":
                                        this.plREMOVE_ATTR.Add(pointToAdd);
                                        break;
                                }
                                break;
                        }
                    }
                }
                return true;
            }
            else 
            {
                return false;
            }
        }

        #endregion  Functions

    }
}
